#! /usr/bin/python

import sys,socket,struct,select

BLOCK_SIZE= 512

OPCODE_RRQ=   1
OPCODE_WRQ=   2
OPCODE_DATA=  3
OPCODE_ACK=   4
OPCODE_ERR=   5

MODE_NETASCII= "netascii"
MODE_OCTET=    "octet"
MODE_MAIL=     "mail"

TFTP_PORT= 6969

# Timeout in seconds
TFTP_TIMEOUT= 2

ERROR_CODES = ["Undef",
               "File not found",
               "Access violation",
               "Disk full or allocation exceeded",
               "Illegal TFTP operation",
               "Unknown transfer ID",
               "File already exists",
               "No such user"]

# Internal defines
TFTP_GET = 1
TFTP_PUT = 2


def make_packet_rrq(filename, mode):
    # Note the exclamation mark in the format string to pack(). What is it for?
    return struct.pack("!H", OPCODE_RRQ) + filename + '\0' + mode + '\0'

def make_packet_wrq(filename, mode):
    return struct.pack("!H", OPCODE_WRQ) + filename + '\0' + mode + '\0'

def make_packet_data(blocknr, data):
    return struct.pack("!HH", OPCODE_DATA,blocknr) + data

def make_packet_ack(blocknr):
    return struct.pack("!HH", OPCODE_ACK, blocknr)

def make_packet_err(errcode, errmsg):
    return struct.pack("!HH", OPCODE_ERR, errcode) + errmsg + '\0'

def parse_packet(msg):
    """This function parses a recieved packet and returns a tuple where the
        first value is the opcode as an integer and the following values are
        the other parameters of the packets in python data types"""
    opcode = struct.unpack("!H", msg[:2])[0]
    if opcode == OPCODE_RRQ:
        l = msg[2:].split('\0')
        if len(l) != 3:
            return None
        return opcode, l[1], l[2]
    elif opcode == OPCODE_WRQ:
        l = msg[2:].split('\0')
        if len(l) != 3:
            return None
        return opcode, l[1], l[2]
    elif opcode == OPCODE_DATA:
        block = msg[2:4]
        data = msg[4:]
        return opcode, struct.unpack("!H", block)[0], data
    elif opcode == OPCODE_ACK:
        block = msg[2:4]
        return opcode, struct.unpack("!H", block)[0]
    elif opcode == OPCODE_ERR:
        errorcode = msg[2:4]
        errormsg = msg[4:]
        return opcode, struct.unpack("!H",errorcode)[0], errormsg
    return None


def tftp_put_transfer(fd, sock, hostaddr):
    put_message = make_packet_wrq(fd.name, MODE_OCTET)
    length_of_file = len(fd.read())
    fd.seek(0)
    numb_of_blocks = (length_of_file/512) + 1 # Number of blocks for the file.

    sock.sendto(put_message, hostaddr)
    block = 0
    counter = 0

    while counter < 10:
        readable = select.select([sock], [], [], TFTP_TIMEOUT)
        #something has been received
        if readable[0]:
            counter = 0
            data, addr = sock.recvfrom(516)
            parser = parse_packet(data)

            if (parser[0] == OPCODE_ACK):
                print block
                # If the ack is for the last packet.
                if (numb_of_blocks == parser[1]):
                    print fd.name + "was successfully transferred to the server."
                    break
                # The right ack has been received.
                elif (block == parser[1]):
                    fd.seek(block*BLOCK_SIZE)
                    # is it the last ack?
                    #if (len(data) < BLOCK_SIZE):
                    if (block + 1 == numb_of_blocks): ### this has been changed ###
                        print "sending last packet.\n"
                    data = fd.read(BLOCK_SIZE)
                    block += 1
                    sock.sendto(make_packet_data(block,data), addr)

            # Error message occurred
            elif (parser[0] == OPCODE_ERR):
                errorcode = parser[1]
                errormsg = parser[2]
                print "Error code : " + str(ERROR_CODES[errorcode]) + "Error message : " + str(errormsg) + ".\n the transfer was unscucessfull."
                sys.exit(1)
        #Timeout
        else:
            counter = counter + 1
            if block == 0:
                sock.sendto(put_message, hostaddr)
                print "Resending put request"
            else:
                fd.seek((block-1)*BLOCK_SIZE)
                data = fd.read(BLOCK_SIZE)
                print "Resending last packet. Block #" + str(block)
                sock.sendto(make_packet_data(block,data),addr)        
    if(counter >= 10):
        print "Connection got timed out."
        sys.exit(1)




def tftp_get_transfer(fd, sock, hostaddr):
    request_message = make_packet_rrq(fd.name, MODE_OCTET)
    sock.sendto(request_message, hostaddr)

    block = 0
    counter = 0
    while counter < 10: 
        readable = select.select([sock], [], [], TFTP_TIMEOUT)
        #something has been received.
        if readable[0]:
            counter = 0
            data, addr = sock.recvfrom(516)
            parser = parse_packet(data)
            
            # Check if we got a data packet.
            if(parser[0] == OPCODE_DATA):
            
                receved_block_nr = parser[1]
                print "Recieved block nr " + str(receved_block_nr)
                # If we get the right block.
                if ((block+1) == receved_block_nr):
                    print "Got the correct block"
                    block = receved_block_nr # update block counter
                    data = parser[2]
                    fd.write(data) # write data to the file
                    sock.sendto(make_packet_ack(block), addr)

                    # If the last data package recieved.
                    if(len(data) < BLOCK_SIZE):
                        print "Sending last ack. \n"
                        break
                else:
                    sock.sendto(make_packet_ack(block), addr)
                    print "Got the wrong block, resending previous ack"

            elif (parser[0] == OPCODE_ERR):
                errorcode = parser[1]
                errormsg = parser[2]
                print "Error " + str(ERROR_CODES[errorcode]) + ". " + str(errormsg)
                sys.exit(1)

        # Timeout
        else:
            #print "Timed out. Resending ACK #" + str(block)
            # if the rrq message got lost.
            counter += 1
            if (block == 0):
                sock.sendto(request_message, hostaddr)
                print "Timed out. Resending file request"
            #else:
            #    sock.sendto(make_packet_ack(block),addr)
            else:
                sock.sendto(make_packet_ack(block),addr)
                print "Timed out. Resending ACK #" + str(block)
    if(counter >= 10):
        print "Connection got timed out."
        sys.exit(1)






def tftp_transfer(fd, hostname, direction):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("", TFTP_PORT))

    try:
        hostaddr = socket.getaddrinfo(hostname, TFTP_PORT, 0, 0, socket.SOL_TCP)[0][4]
    except socket.gaierror, e:
        print "Addresss-related error connecting to server: %s" %e
        sys.exit(1)

    if (direction == TFTP_PUT):
        tftp_put_transfer(fd, sock, hostaddr)
    else:
        tftp_get_transfer(fd, sock, hostaddr)




def usage():
    """Print the usage on stderr and quit with error code"""
    sys.stderr.write("Usage: %s [-g|-p] FILE HOST\n" % sys.argv[0])
    sys.exit(1)


def main():
    # No need to change this function
    direction = TFTP_GET
    if len(sys.argv) == 3:
        filename = sys.argv[1]
        hostname = sys.argv[2]
    elif len(sys.argv) == 4:
        if sys.argv[1] == "-g":
            direction = TFTP_GET
        elif sys.argv[1] == "-p":
            direction = TFTP_PUT
        else:
            usage()
            return
        filename = sys.argv[2]
        hostname = sys.argv[3]
    else:
        usage()
        return

    if direction == TFTP_GET:
        print "Transfer file %s from host %s" % (filename, hostname)
    else:
        print "Transfer file %s to host %s" % (filename, hostname)

    try:
        if direction == TFTP_GET:
            fd = open(filename, "wb")
        else:
            fd = open(filename, "rb")
    except IOError as e:
        sys.stderr.write("File error (%s): %s\n" % (filename, e.strerror))
        sys.exit(2)

    tftp_transfer(fd, hostname, direction)
    fd.close()

if __name__ == "__main__":
    main()