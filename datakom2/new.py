#! /usr/bin/python

import sys,socket,struct,select
#import hashlib

BLOCK_SIZE= 512

OPCODE_RRQ=   1
OPCODE_WRQ=   2
OPCODE_DATA=  3
OPCODE_ACK=   4
OPCODE_ERR=   5

MODE_NETASCII= "netascii"
MODE_OCTET=    "octet"
MODE_MAIL=     "mail"

TFTP_PORT= 6969

# Timeout in seconds
TFTP_TIMEOUT= 2

ERROR_CODES = ["Undef",
               "File not found",
               "Access violation",
               "Disk full or allocation exceeded",
               "Illegal TFTP operation",
               "Unknown transfer ID",
               "File already exists",
               "No such user"]

# Internal defines
TFTP_GET = 1
TFTP_PUT = 2


def make_packet_rrq(filename, mode):
    # Note the exclamation mark in the format string to pack(). What is it for?
    return struct.pack("!H", OPCODE_RRQ) + filename + '\0' + mode + '\0'

def make_packet_wrq(filename, mode):
    return struct.pack("!H", OPCODE_WRQ) + filename + '\0' + mode + '\0'

def make_packet_data(BLOCKnr, data):
    return struct.pack("!HH", OPCODE_DATA,BLOCKnr) + data

def make_packet_ack(BLOCKnr):
    return struct.pack("!HH", OPCODE_ACK, BLOCKnr)

def make_packet_err(errcode, errmsg):
    return struct.pack("!HH", OPCODE_ERR, errcode) + errmsg + '\0'

def parse_packet(msg):
    """This function parses a recieved packet and returns a tuple where the
        first value is the opcode as an integer and the following values are
        the other parameters of the packets in python data types"""
    opcode = struct.unpack("!H", msg[:2])[0]
    if opcode == OPCODE_RRQ:
        l = msg[2:].split('\0')
        if len(l) != 3:
            return None
        return opcode, l[1], l[2]

    elif opcode == OPCODE_WRQ:
        l = msg[2:].split('\0')
        if len(l) != 3:
            return None
        return opcode, l[1], l[2]

    elif opcode == OPCODE_DATA:
        BLOCK = msg[2:4]
        data = msg[4:]
        return opcode, struct.unpack("!H", BLOCK)[0], data

    elif opcode == OPCODE_ACK:
        BLOCK = msg[2:4]
        return opcode, struct.unpack("!H", BLOCK)[0]

    elif opcode == OPCODE_ERR:
        errorcode = msg[2:4]
        errormsg = msg[4:]
        return opcode, struct.unpack("!H",errorcode)[0], errormsg
    return None


###### Get ######
def tftp_get_transfer(fd, sock, HOST_ADDR):
    request_message = make_packet_rrq(fd.name, MODE_OCTET)
    sock.sendto(request_message, HOST_ADDR)

    BLOCK = 1
    TRIES = 0
    while TRIES < 10: 
        readable = select.select([sock], [], [], TFTP_TIMEOUT)
        #something has been received.
        if readable[0]:
            TRIES = 0
            RECEIVED_PKT, HOST_ADDR = sock.recvfrom(516)
            #parser = parse_packet(data)
            pkt = parse_packet(RECEIVED_PKT)

            (opcode, _, _) = pkt

            # Check if we got a data packet.
            if(opcode == OPCODE_DATA):
                (opcode, blocknr, data) = pkt
            
                RECEIVED_BLOCK = blocknr
                print "Recieved BLOCK nr " + str(RECEIVED_BLOCK)

                # If we get the right BLOCK.
                if (BLOCK == RECEIVED_BLOCK):
                    print "Got the correct BLOCK"
                    BLOCK = RECEIVED_BLOCK # update BLOCK TRIES
                    fd.write(data) # write data to the file
                    sock.sendto(make_packet_ack(BLOCK), HOST_ADDR)

                    # If the last data package recieved.
                    if(len(data) < BLOCK_SIZE):
                        print "Sending last ack"
                        break
                else:
                    print "Got the wrong BLOCK, resending previous ack"
                    sock.sendto(make_packet_ack(BLOCK), HOST_ADDR)
                    

            elif (opcode == OPCODE_ERR):
                (opcode, errcode, errmessage) = pkt
                print "Error " + str(ERROR_CODES[errcode]) + ". " + str(errmessage)
                sys.exit(1)

        # Timeout
        else:
            # if the rrq message got lost.
            TRIES += 1
            if (BLOCK == 1):
                print "Timed out. Resending file request"
                sock.sendto(request_message, HOST_ADDR)

            else:
                print "Timed out. Resending ACK #" + str(BLOCK)
                sock.sendto(make_packet_ack(BLOCK),HOST_ADDR)

    if(TRIES >= 10):
        print "Connection got timed out."
        sys.exit(1)

###### Put ######
def tftp_put_transfer(fd, sock, HOST_ADDR):
    put_message = make_packet_wrq(fd.name, MODE_OCTET)
    LENGTH_OF_FILE = len(fd.read())
    fd.seek(0)
    NUMB_OF_BLOCKS = (LENGTH_OF_FILE/512) + 1 # Number of BLOCKs for the file.

    sock.sendto(put_message, HOST_ADDR)
    BLOCK = 0
    TRIES = 0

    while TRIES < 10:
        readable = select.select([sock], [], [], TFTP_TIMEOUT)
        #something has been received
        if readable[0]:
            TRIES = 0
            RECEIVED_PKT, HOST_ADDR = sock.recvfrom(516)
            pkt = parse_packet(RECEIVED_PKT)
            (opcode, _, _) = pkt

            if (opcode == OPCODE_ACK):
                (opcode, blocknr, _) = pkt 
                #print BLOCK
                # If the ack is for the last packet.
                if (NUMB_OF_BLOCKS == blocknr):
                    print fd.name + "was successfully transferred to the server."
                    break

                # The right ack has been received.
                elif (BLOCK == blocknr):
                    fd.seek(BLOCK*BLOCK_SIZE)

                    # is it the last ack?
                    if (BLOCK + 1 == NUMB_OF_BLOCKS): 
                        print "sending last packet.\n"
                    data = fd.read(BLOCK_SIZE)
                    BLOCK += 1
                    sock.sendto(make_packet_data(BLOCK,data), HOST_ADDR)

            # Error message occurred
            elif (opcode == OPCODE_ERR):
                (opcode, errcode, errmessage) = pkt
                print "Error code : " + str(ERROR_CODES[errcode]) + "Error message : " + str(errmessage)
                sys.exit(1)

        #Timeout
        else:
            TRIES += 1
            if BLOCK == 0:
                sock.sendto(put_message, hostHOST_ADDR)
                print "Resending put request"

            else:
                fd.seek((BLOCK-1)*BLOCK_SIZE)
                data = fd.read(BLOCK_SIZE)
                print "Resending last packet. BLOCK #" + str(BLOCK)
                sock.sendto(make_packet_data(BLOCK,data),HOST_ADDR) 

    if(TRIES >= 10):
        print "Connection got timed out."
        sys.exit(1)


def tftp_transfer(fd, hostname, direction):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("", TFTP_PORT))

    try:
        HOST_ADDR = socket.getaddrinfo(hostname, TFTP_PORT, 0, 0, socket.SOL_TCP)[0][4]
    except socket.gaierror, e:
        print "Host addresss related error connecting to server: %s" %e
        sys.exit(1)

    if (direction == TFTP_PUT):
        tftp_put_transfer(fd, sock, HOST_ADDR)
    else:
        tftp_get_transfer(fd, sock, HOST_ADDR)




def usage():
    """Print the usage on stderr and quit with error code"""
    sys.stderr.write("Usage: %s [-g|-p] FILE HOST\n" % sys.argv[0])
    sys.exit(1)


def main():
    # No need to change this function
    direction = TFTP_GET
    if len(sys.argv) == 3:
        filename = sys.argv[1]
        hostname = sys.argv[2]
    elif len(sys.argv) == 4:
        if sys.argv[1] == "-g":
            direction = TFTP_GET
        elif sys.argv[1] == "-p":
            direction = TFTP_PUT
        else:
            usage()
            return
        filename = sys.argv[2]
        hostname = sys.argv[3]
    else:
        usage()
        return

    if direction == TFTP_GET:
        print "Transfer file %s from host %s" % (filename, hostname)
    else:
        print "Transfer file %s to host %s" % (filename, hostname)

    try:
        if direction == TFTP_GET:
            fd = open(filename, "wb")
        else:
            fd = open(filename, "rb")
    except IOError as e:
        sys.stderr.write("File error (%s): %s\n" % (filename, e.strerror))
        sys.exit(2)

    tftp_transfer(fd, hostname, direction)
    fd.close()

    # try:
    #     if direction == TFTP_GET:
    #         with open(filename, "rb") as fd:
    #             d = fd.read()

    #         md5 = hashlib.md5(d).hexdigest()
    #         print md5
    #         print str(len(d))
    #         if filename == "small.txt":
    #             true_md5 = "667ff61c0d573502e482efa85b468f1f"
    #             true_size = 1931
    #         elif filename == "medium.pdf":
    #             true_md5 = "ee98d0524433e2ca4c0c1e05685171a7"
    #             true_size = 17577
    #         elif filename == "large.jpeg":
    #             true_md5 = "f5b558fe29913cc599161bafe0c08ccf"
    #             true_size = 82142

    #         if md5 == true_md5 and len(d) == true_size:
    #             print ">> True <<"
    #         else:
    #             print ">> False <<"

    # except IOError as e:
    #     sys.stderr.write("File error (%s): %s\n" % (filename, e.strerror))
    #     sys.exit(2)

if __name__ == "__main__":
    main()

