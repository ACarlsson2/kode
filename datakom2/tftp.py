#! /usr/bin/python
# https://pymotw.com/2/struct/
import sys,socket,struct,select,hashlib,time


returned_packet_SIZE= 512

# 512 + header (4)

#TFTP header consists of a 2 byte opcode field which indicates the packet's type
OPCODE_RRQ = 1
OPCODE_WRQ = 2
OPCODE_DATA = 3
OPCODE_ACK = 4
OPCODE_ERR = 5

MODE_NETASCII= "netascii"
MODE_OCTET=    "octet"
MODE_MAIL=     "mail"

# Timeout in seconds
TFTP_TIMEOUT= 2

ERROR_CODES = ["Undef",
               "File not found",
               "Access violation",
               "Disk full or allocation exceeded",
               "Illegal TFTP operation",
               "Unknown transfer ID",
               "File already exists",
               "No such user"]

# Internal defines
TFTP_GET = 1
TFTP_PUT = 2

"""
\0 is a blank space
H => C type: unsigned short, Python type: integer, Standard size: 2
""" 
def make_packet_rrq(filename, mode):
    # Note the exclamation mark in the format string to pack(). What is it for?
    # ! = network, H = unsigned short
    return struct.pack("!H", OPCODE_RRQ) + filename + '\0' + mode + '\0'

def make_packet_wrq(filename, mode):
    return struct.pack("!H", OPCODE_WRQ) + filename + '\0' + mode + '\0'

def make_packet_data(blocknr, data):
    # !HH
    return struct.pack("!HH", OPCODE_DATA, blocknr) + data

def make_packet_ack(blocknr):
    return struct.pack("!HH", OPCODE_ACK, blocknr) 

def make_packet_err(errcode, errmsg):
    return struct.pack("!HH", OPCODE_ERR, errcode) + errmsg + '\0'

def parse_packet(msg):
    """This function parses a recieved packet and returns a tuple where the
        first value is the opcode as an integer and the following values are
        the other parameters of the packets in python data types"""
    opcode = struct.unpack("!H", msg[:2])[0]
    if opcode == OPCODE_RRQ or opcode == OPCODE_WRQ:
        l = msg[2:].split('\0')
        if len(l) != 3:
            return None
        return opcode, l[1], l[2]
    
    elif opcode == OPCODE_DATA:
        returned_packet = struct.unpack("!H", msg[2:4])[0]
        return opcode, returned_packet, msg[4:] 
    
    elif opcode == OPCODE_ACK:
        returned_packet = struct.unpack("!H", msg[2:])[0]
        return opcode, returned_packet, msg[4:]
    
    elif opcode == OPCODE_ERR:
        errcode = struct.unpack("!H", msg[2:4])[0]
        errmsg = msg[4:-1]
        return opcode, errcode, errmsg
    else:
        return None 



def tftp_transfer(fd, hostname, direction):
    # Variables
    #server_url              = "joshua.it.uu.se"
    server_url              = "rabbit.it.uu.se"
    hostname = server_url

    PORT_SERVER             = 69
    PORT_PUBLIC             = 6969
    PORT_PACKAGE_LOSS       = 10069
    PORT_PACKAGE_LOSS_THIRY = 13069
    PORT_PACKAGE_DUPLICATE  = 20069
    SERVER_ADDR             = socket.getaddrinfo(hostname, PORT_SERVER) #[0][4:][0]
    SERVER_ADDR_IP          = SERVER_ADDR[0][4][0]
    SERVER_ADDR_PORT        = SERVER_ADDR[0][4][1]

    #READ_SIZE               = 1024 # 516
    #READ_SIZE               = 516
    DONE_SENDING            = False
    UPDATED_ADDR            = False
    server_TID              = None
    last_packet             = False

    # Open socket interface
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP

    # Check if we are putting a file or getting a file and create 
    # the corresponding packet and send it
    if direction == TFTP_GET:
        READ_DATA = returned_packet_SIZE + 4
        BLOCK_NUMBER = 1
        package_read_write = make_packet_rrq(fd.name, MODE_OCTET)
        print "Sending RRQ packet"
        print "direction == TFTP_GET, time2read"

    elif direction == TFTP_PUT:
        READ_DATA = 4
        BLOCK_NUMBER = 0
        package_read_write = make_packet_wrq(fd.name, MODE_OCTET)

        open_and_read_file = fd.read()
        bytes_left_on_file = len(open_and_read_file)
        print "direction == TFTP_PUT, time2write"
        print "Sending WRQ packet"
    else:
        print "error in direction"

    # Send the just created packet
    (bytes) = s.sendto(package_read_write, (SERVER_ADDR_IP, SERVER_ADDR_PORT))
    
    # Put or get the file, returned_packet by returned_packet, in a loop.
    while True:

        # Listen and wait until received a packet

        readable, writable, exceptional = select.select([s], [], [], TFTP_TIMEOUT) # Waiting for I/O completion
        print "readable: {}".format(readable)
        try: 

            # If received from server, read and unpack the packet
            if s in readable:
                (returned_packet, returned_packet_addr) = s.recvfrom(READ_DATA)
                (HOST_ADDR_IP, HOST_ADDR_PORT) = returned_packet_addr
                pkt = parse_packet(returned_packet)
                print pkt
                print "Reading stuff woho! :)"

                if UPDATED_ADDR == False:
                    SERVER_ADDR_IP = HOST_ADDR_IP
                    SERVER_ADDR_PORT = HOST_ADDR_PORT
                    UPDATED_ADDR = True
                    print "SERVER_ADDR updated"

                if SERVER_ADDR_IP != HOST_ADDR_IP:
                    package_err = make_packet_err(5, ERROR_CODES[5])
                    socket_client.sendto(package_err, (SERVER_ADDR_IP, SERVER_ADDR_PORT))
                    print "ERR5: ERROR INVALID DESTINATION"
                    continue

                #error
                (opcode, errcode, errmessage) = pkt
                if opcode == OPCODE_ERR:
                    print "ERROR | opcode: {}, errcode: {}, errmessage: {}".format(opcode, ERROR_CODES[errcode], errmessage)
                    break


                # GET ------
                elif opcode == OPCODE_DATA and direction == TFTP_GET:
                    (opcode, blocknr, data) = pkt
                    print "RECEIVED DATA blocknr " + str(blocknr)
                    
                    # If received correct DATA returned_packet, write data to file and send ACK
                    if BLOCK_NUMBER == blocknr:
                        resend_count = 0
                        fd.write(data)
                        packet = make_packet_ack(blocknr)
                        BLOCK_NUMBER += 1

                        # If last packet, set the last_packet-flag TRUE
                        if len(returned_packet) < returned_packet_SIZE + 4: 
                            last_packet = True

                        print "Sending ACK for returned_packet: " + str(blocknr)
                        print "------------------------------------"


                    # Else, received wrong (a duplicate) DATA returned_packet, RE-Send the previous ACK
                    else:
                        packet = make_packet_ack(blocknr)
                        print "DUPLICATE PACKET, resend ack"
                        resend_count += 1


                # PUT ----
                elif opcode == OPCODE_ACK and direction == TFTP_PUT:

                    (opcode, blocknr, _) = pkt 
                    print "RECEIVED ACK blocknr: " + str(blocknr)
                    
                    # If received correct ACK returned_packet, create next DATA returned_packet
                    if BLOCK_NUMBER == blocknr: 
                        resend_count = 0
                        BLOCK_NUMBER += 1
                        blocknr += 1

                        # If last DATA packet to send, set the last_packet-flag TRUE
                        if(bytes_left < returned_packet_SIZE):
                            n_bytes = bytes_left
                            bytes_left -= bytes_left
                            msg = "Sending last"
                            if bytes_left == 0:

                                last_packet = True
                            
                        # Else create normal returned_packet_size'd DATA packet
                        else:     
                            n_bytes = returned_packet_SIZE
                            bytes_left -= returned_packet_SIZE
                            msg = "Sending"
                        
                        data_returned_packet = read_file[(returned_packet_SIZE * BLOCK_NUMBER) : (returned_packet_SIZE * BLOCK_NUMBER) + n_bytes]
                        packet = make_packet_data(blocknr, data_returned_packet)
                        print msg + " returned_packet: " + str(blocknr)
                        print "------------------------------------"
                        
                    
                    # Else, received wrong ACK returned_packet, RE-create the previous DATA returned_packet
                    else:
                        packet = make_packet_data(BLOCK_NUMBER, read_file[(returned_packet_SIZE * BLOCK_NUMBER): (returned_packet_SIZE * BLOCK_NUMBER)+ returned_packet_SIZE])
                        resend_count += 1
                        print "Duplicate!"
                        print "Resending returned_packet: " + str(BLOCK_NUMBER) + ", resend count: " + str(resend_count)
                        print "------------------------------------"
                    

                # SEND PACKET MADE IN GET OR PUT ---------------
                readable, writable, exceptional = select.select([], [s], [], 1) # Waiting for I/O completion
                print "writable: {}".format(writable)

                if s in writable: 
                    (bytes) = s.sendto(packet, (SERVER_ADDR_IP, SERVER_ADDR_PORT))
                    if last_packet == True: 
                        break
                else:
                    print "Send packet timed out!"


            # Initial request timed out
            else:
                # If RRQ packet was lost, resend
                if direction == TFTP_GET and BLOCK_NUMBER == 1:
                    (bytes) = s.sendto(packet, (SERVER_ADDR_IP, SERVER_ADDR_PORT))
                    print "Timeout! Resent RRQ packet"

                # Else if WRQ packet was lost, resend
                elif direction == TFTP_PUT and BLOCK_NUMBER == 0:
                    (bytes) = s.sendto(packet, (SERVER_ADDR_IP, SERVER_ADDR_PORT))
                    print "Timeout! Resent WRQ packet"

                # If failed to resend packet too many times and the server stop responding
                elif resend_count > 3:
                    print "Retried to send packet to many times. Restart request"
                    if direction == TFTP_GET:
                        BLOCK_NUMBER = 1
                        p = make_packet_rrq(fd.name, MODE_OCTET)
                    elif direction == TFTP_PUT:
                        BLOCK_NUMBER = 0
                        p = make_packet_wrq(fd.name, MODE_OCTET)
                    else:
                        print "Invalid direction"
                    fd.seek(0)
                    (bytes) = s.sendto(p, (SERVER_ADDR_IP, SERVER_ADDR_PORT))

                # No response yet
                else:
                    print "Timeout!"

    # EXCEPTION ---------------------------------------------------------------------------           
        except: 
            print "Exception!!"
    fd.close()


def usage():
    """Print the usage on stderr and quit with error code"""
    sys.stderr.write("Usage: %s [-g|-p] FILE HOST\n" % sys.argv[0])
    sys.exit(1)

# def main():

#     # No need to change this function
#     # select
#     direction = TFTP_GET
#     if len(sys.argv) == 3:
#         filename = sys.argv[1]
#         hostname = sys.argv[2]
#     elif len(sys.argv) == 4:
#         if sys.argv[1] == "-g":
#             direction = TFTP_GET
#         elif sys.argv[1] == "-p":
#             direction = TFTP_PUT
#         else:
#             usage()
#             return
#         filename = sys.argv[2]
#         hostname = sys.argv[3]
#     else:
#         usage()
#         return

#     if direction == TFTP_GET:
#         print "Transfer file %s from host %s" % (filename, hostname)
#     else:
#         print "Transfer file %s to host %s" % (filename, hostname)

#     try:
#         if direction == TFTP_GET:
#             fd = open(filename, "wb")
#         else:
#             fd = open(filename, "rb")
#     except IOError as e:
#         sys.stderr.write("File error (%s): %s\n" % (filename, e.strerror))
#         sys.exit(2)

#     tftp_transfer(fd, hostname, direction)
#     fd.close()

#     try:
#         if direction == TFTP_GET:
#             with open(filename, "rb") as fd:
#                 d = fd.read()

#             md5 = hashlib.md5(d).hexdigest()
#             print md5
#             print str(len(d))
#             if filename == "small.txt":
#                 true_md5 = "667ff61c0d573502e482efa85b468f1f"
#                 true_size = 1931
#             elif filename == "medium.pdf":
#                 true_md5 = "ee98d0524433e2ca4c0c1e05685171a7"
#                 true_size = 17577
#             elif filename == "large.jpeg":
#                 true_md5 = "f5b558fe29913cc599161bafe0c08ccf"
#                 true_size = 82142

#             if md5 == true_md5 and len(d) == true_size:
#                 print "True"
#             else:
#                 print "False"

#     except IOError as e:
#         sys.stderr.write("File error (%s): %s\n" % (filename, e.strerror))
#         sys.exit(2)


# if __name__ == "__main__":
#     main()
def main():
    # No need to change this function
    direction = TFTP_GET
    if len(sys.argv) == 3:
        filename = sys.argv[1]
        hostname = sys.argv[2]
    elif len(sys.argv) == 4:
        if sys.argv[1] == "-g":
            direction = TFTP_GET
        elif sys.argv[1] == "-p":
            direction = TFTP_PUT
        else:
            usage()
            return
        filename = sys.argv[2]
        hostname = sys.argv[3]
    else:
        usage()
        return

    if direction == TFTP_GET:
        print "Transfer file %s from host %s" % (filename, hostname)
    else:
        print "Transfer file %s to host %s" % (filename, hostname)

    try:
        if direction == TFTP_GET:
            fd = open(filename, "wb")
        else:
            fd = open(filename, "rb")
    except IOError as e:
        sys.stderr.write("File error (%s): %s\n" % (filename, e.strerror))
        sys.exit(2)

    tftp_transfer(fd, hostname, direction)
    fd.close()

    try:
        if direction == TFTP_GET:
            with open(filename, "rb") as fd:
                d = fd.read()

            md5 = hashlib.md5(d).hexdigest()
            print md5
            print str(len(d))
            if filename == "small.txt":
                true_md5 = "667ff61c0d573502e482efa85b468f1f"
                true_size = 1931
            elif filename == "medium.pdf":
                true_md5 = "ee98d0524433e2ca4c0c1e05685171a7"
                true_size = 17577
            elif filename == "large.jpeg":
                true_md5 = "f5b558fe29913cc599161bafe0c08ccf"
                true_size = 82142

            if md5 == true_md5 and len(d) == true_size:
                print ">> True <<"
            else:
                print ">> False <<"

    except IOError as e:
        sys.stderr.write("File error (%s): %s\n" % (filename, e.strerror))
        sys.exit(2)

if __name__ == "__main__":
    main()