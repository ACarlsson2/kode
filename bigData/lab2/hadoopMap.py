#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""A more advanced Mapper, using Python iterators and generators."""

import sys
import json

def read_input(file):
    for line in file:
        # split the line into words... not
        yield line

def main(separator='\t'):
    # input comes from STDIN (standard input)
    data = read_input(sys.stdin)
    for tweet in data:
        #print tweet
        if len(tweet) > 1:
            json_tweet = json.loads(tweet)
            if "retweeted_status" not in json_tweet:
                print "%s%s%s" % ("unique", separator, 1)
                line = json_tweet["text"].lower().split()
                for word in line:
                    if word == "han" or word == "hon" or word == "hen" or word == "den" or word == "denna" or word == "denne" or word == "det":
                        # write the results to STDOUT (standard output);
                        # what we output here will be the input for the
                        # Reduce step, i.e. the input for reducer.py
                        print "%s%s%s" % (word, separator, 1)


if __name__ == "__main__":
    main()
