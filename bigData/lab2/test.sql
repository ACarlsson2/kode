DROP TABLE IF EXISTS raw_json;
DROP TABLE IF EXISTS tweet_text;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_json(json string)
ROW FORMAT DELIMITED
STORED AS textfile
location '/tweets/';

CREATE TABLE IF NOT EXISTS tweet_text(text string);

INSERT INTO tweet_text (text)
SELECT get_json_object(raw_json.json, '$.text')
FROM raw_json
WHERE get_json_object(raw_json.json, '$.retweeted_status') IS NULL and get_json_object(raw_json.json, '$.text') IS NOT NULL;


SELECT word, COUNT(*)
FROM tweet_text LATERAL VIEW explode(split(lower(text), ' ')) lTable as word
WHERE word = 'han' or word = 'hon' or word = 'hen' or word = 'den' or word = 'det' or word = 'denna' or word = 'denne'
GROUP BY word;
