import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Adventcoins{

    private static String input = "iwrupvqb";
    private static int hashInt = 0;

    public static void main(String arg[]){

        while(true){
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] messageDigest = md.digest((input+hashInt).getBytes());
                BigInteger number = new BigInteger(1, messageDigest);
                String hashtext = number.toString(16);
                // Now we need to zero pad it if you actually want the full 32 chars.
                    while (hashtext.length() < 32) {
                        hashtext = "0" + hashtext;
                    }

                    if(hashtext.substring(0,6).equals("000000")) {
                        System.out.println(hashtext);
                        System.out.printf("%d\n", hashInt);
                        break;
                    }
                    else {
                        hashInt++;
                    }
                }
                catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
            
        }

    }
}