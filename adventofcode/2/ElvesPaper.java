import java.util.Arrays;
import java.io.*;

public class ElvesPaper{
    private static String test = "2x3x4";
    private static int paper = 0;
    private static int ribbon = 0;

    public static void main(String arg[]) throws IOException{

        String line;
        BufferedReader in;

        in = new BufferedReader(new FileReader("dims.txt"));
        line = in.readLine();

        while(line != null){


            String[] sDims = line.split("x");
            int w = Integer.parseInt(sDims[0]);
            int l = Integer.parseInt(sDims[1]);
            int h = Integer.parseInt(sDims[2]);

            
            int[] iDims = {w,l,h};
            Arrays.sort(iDims);

           
            paper += calculatePaper(iDims);
            ribbon += calculateRibbon(iDims);
            //System.out.printf("%d\n", calculatePaper(w,l,h));
            line = in.readLine();
        }

        System.out.printf("Total paper needed: %d\n", paper);
        System.out.printf("Total ribbon needed: %d\n", ribbon);

    }

    private static int calculatePaper(int[] dims){
        
        int need = 2*dims[0]*dims[1] + 2*dims[1]*dims[2] + 2*dims[0]*dims[2] +dims[0]*dims[1];
        return need;
    }

    private static int calculateRibbon(int[] dims){
        int need = 2*dims[0] + 2*dims[1] + dims[0]*dims[1]*dims[2];

        return need;
    }
}