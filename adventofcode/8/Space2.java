import java.io.*;
import java.util.*;

public class Space2{
    private static int encoded = 0;
    private static int characters = 0;

    public static void main(String args[]) throws FileNotFoundException {
        Scanner input = new Scanner(new File("input.txt"));
        String next;

        while(input.hasNextLine()) {
            encoded += 2;
            next = input.nextLine();
            
            // räkna characters
            for(int i = 0; i < next.length(); i++) {
                characters++;
                encoded++;
                if (next.charAt(i) == 92 || next.charAt(i) == 34) {
                    encoded += 1;
                    
                }
                // System.out.print(next.charAt(i));
                // System.out.println("");
            }
            //next = fixEncoding(next);
            // räna encoded
            //for(int i = 0; i < next.length(); i++) {
              //  encoded++;
                // System.out.print(next.charAt(i));
                // System.out.println("");
            //}

}
            
        System.out.println("encoded = " + encoded);
        System.out.println("chars = " + characters);
        System.out.println("Answer = " + (encoded - characters));

        // System.out.printf("%c\n", Integer.parseInt("27", 16));
    }
    public static String fixEncoding(String str) {
        System.out.println("pre: " + str);
        str = fixQuote(str);
        str = fixSlash(str);

        
        System.out.println("after: " + str);
        return str; 
    }
    public static String fixSlash (String str) {
        String tmp = str;
        for (int i = 0; i < str.length(); i++) {
            encoded++;
            // char is \
            if (str.charAt(i) == 92) {
               // tmp = tmp.substring(0,i) + "\\\\"  + tmp.substring(i+1);
                encoded+=2;
            }
        }
        return tmp;
    }

    public static String fixQuote (String str) {
        String tmp = str;
        for (int i = 0; i < str.length(); i++) {
            encoded++;
            // char is ""
            if (str.charAt(i) == 34) {
                //tmp = tmp.substring(0,i) + "\""  + tmp.substring(i+1); 
                encoded += 2;
            }
        }
        return tmp;
    }
}