import java.io.*;
import java.util.*;

public class Space{
    private static int code = 0;
    private static int characters = 0;

    public static void main(String args[]) throws FileNotFoundException {
        Scanner input = new Scanner(new File("input.txt"));
        String next;

        while(input.hasNextLine()) {
            next = input.nextLine();

            // räkna code
            for(int i = 0; i < next.length(); i++) {
                code++;
                // System.out.print(next.charAt(i));
                // System.out.println("");
            }

            // räkna characters
            next = next.substring(1, next.length()-1);
            for (int i = 0; i < next.length(); i++) {
                // vanligt tecken
                if (next.charAt(i) >= 97 && next.charAt(i) <= 122) {
                    characters++;
                }
                // tecken är \
                else if (next.charAt(i) == 92) {
                    i++;
                    // char är \
                    if (next.charAt(i) == 92) {
                        characters++;
                    }
                    // char är "
                    else if (next.charAt(i) == 34) {
                        characters++;
                    }
                    // char är x -> hex
                    else if (next.charAt(i) == 120) {
                        characters++;
                        i += 2;
                    }

                }
            }

            System.out.println(next);

        }
        System.out.println("code = " + code);
        System.out.println("chars = " + characters);
        System.out.println("Answer = " + (code - characters));

        // System.out.printf("%c\n", Integer.parseInt("27", 16));
    }
}
