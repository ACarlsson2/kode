import java.io.*;
import java.util.*;


public class Lights{
    private static Map<String, Integer> lightMap = new HashMap<String, Integer>();
    private static int ligtsOn = 0;


    public static void main(String args[]) throws FileNotFoundException{
        Scanner input = new Scanner(new File("input.txt"));

        while(input.hasNextLine()){
            String line = input.nextLine();
            String[] lines = line.split(" ");

            if(lines[0].equals("turn")){
                if(lines[1].equals("on")){
                    // turn on
                    turnOn(lines[2].split(","), lines[4].split(","));
                    //System.out.println("turn on");
                }
                else {
                    // turn off
                    turnOff(lines[2].split(","), lines[4].split(","));
                    //System.out.println("turn off");
                }
            }
            else {
                //toggle
                toggle(lines[1].split(","), lines[3].split(","));
                //System.out.println("toggle");
            }

        }
        System.out.println("Lights on: " + ligtsOn);
    }

    public static void turnOn(String[] start, String[] stop){
        //System.out.println("Turn on: " + x + " " + y);
        int xStart = Integer.parseInt(start[0]);
        int yStart = Integer.parseInt(start[1]);
        int xStop = Integer.parseInt(stop[0]);
        int yStop = Integer.parseInt(stop[1]);

        for (int i = xStart; i<=xStop; i++) {
            for (int j = yStart; j<=yStop; j++) {
                if(lightMap.containsKey(i+","+j)){
                    if(lightMap.get(i+","+j) == 0){
                        lightMap.put(i+","+j, 1);
                        ligtsOn++;
                    }
                }
                else{
                    lightMap.put(i+","+j, 1);
                    ligtsOn++;
                }
                
            }
            
        }

    }

    public static void turnOff(String[] start, String[] stop){
        int xStart = Integer.parseInt(start[0]);
        int yStart = Integer.parseInt(start[1]);
        int xStop = Integer.parseInt(stop[0]);
        int yStop = Integer.parseInt(stop[1]);

        for (int i = xStart; i<=xStop; i++) {
            for (int j = yStart; j<=yStop; j++) {
                if(lightMap.containsKey(i+","+j)){
                    if(lightMap.get(i+","+j) == 1){
                        lightMap.put(i+","+j, 0);
                        ligtsOn--;
                    }
                }
              
            }
            
        }
        
    }

    public static void toggle(String start[], String stop[]){
        int xStart = Integer.parseInt(start[0]);
        int yStart = Integer.parseInt(start[1]);
        int xStop = Integer.parseInt(stop[0]);
        int yStop = Integer.parseInt(stop[1]);

        for (int i = xStart; i<=xStop; i++) {
            for (int j = yStart; j<=yStop; j++) {
                if (lightMap.containsKey(i+","+j)) {

                    if (lightMap.get(i+","+j) == 0) {
                        lightMap.put(i+","+j, 1);
                        ligtsOn++;
                    }
                    else{
                        lightMap.put(i+","+j, 0);
                        ligtsOn--;
                    }
                }
                else{
                    lightMap.put(i+","+j, 1);
                    ligtsOn++;
                    }
            }
            
        }
        
    }
}