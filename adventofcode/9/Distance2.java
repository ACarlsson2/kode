import java.io.*;
import java.util.*;

public class Distance2 {
	private static Map<String, Integer> distanceMap = new HashMap<String, Integer>(); // save
	private static ArrayList<String> locations = new ArrayList<String>();	// all different locations
	private static ArrayList<ArrayList<String>> paths = new ArrayList<ArrayList<String>>(); // all different paths
	private static int shortest = 0;

	public static void main(String args[]) throws FileNotFoundException {
		Scanner input = new Scanner(new File("input.txt"));



        // create distanceMap
        while(input.hasNextLine()) {
      	String[] next = input.nextLine().split(" ");

					// add both x,y and y,x
        	distanceMap.put(next[0] + "," + next[2], Integer.parseInt(next[4]));
					distanceMap.put(next[2] + "," + next[0], Integer.parseInt(next[4]));

					// adding all different locations
					if (!locations.contains(next[0])) {
						locations.add(next[0]);
					}
					if (!locations.contains(next[2])) {
						locations.add(next[2]);
					}
        }

				// crate all permutations of locations
				permutation(locations, 0, locations.size());
				//System.out.println(paths);

				for (int j = 0; j < paths.size(); j++) {
					ArrayList current = paths.get(j);
					System.out.println(current);
					int tmp = 0;
					for (int i = 0; i < current.size()-1; i++) {
						tmp += distanceMap.get(current.get(i) + "," + current.get(i+1));

					}
					//System.out.println(tmp);
					if (shortest == 0) {
						shortest = tmp;
					}
					 else if(tmp > shortest) {
						shortest = tmp;
					}

					//System.out.println(tmp);
				}
				System.out.println(shortest);

	}
	public static void permutation(ArrayList<String> list, int i, int n) {
		// a b c
		if (i == n) {
				paths.add(new ArrayList<String>(list));
			//System.out.println(list);
		}
		else {
			for (int j = i; j<n; j++) {
				String a = list.get(i);
				String b = list.get(j);
				list.set(i,b);
				list.set(j,a);
				permutation(list, i+1, n);
				a = list.get(i);
				b = list.get(j);
				list.set(i,b);
				list.set(j,a);

			}
		}
	}
}
