import java.io.*;
import java.util.*;

public class Logic2{
    private static Map<String, Integer> valueMap = new HashMap<String, Integer>(); // save variable data
    private static List<String> rows = new ArrayList<String>(); // save input file as rows

    public static void main(String[] args) throws FileNotFoundException{
        createRow();
        int a = calculateA();
        valueMap.clear();

        createRow();
        for (int i = 0; i < rows.size()-1; i++) {
            if (rows.get(i).endsWith("-> b")) {
                rows.remove(i);
            }
            
        }
        rows.add(a + " -> b");

        int newa = calculateA();
        System.out.println("a = " + newa);
        
        
    }
    public static void createRow() throws FileNotFoundException {
        Scanner input = new Scanner(new File("input.txt"));

        String next;
        while(input.hasNextLine()) {
            next = input.nextLine();

            //System.out.println(next);
            rows.add(next); // saves file in arraylist
        }

    }

    public static int calculateA() {


        

        int i = 0;
        while(!rows.isEmpty()) {
            // System.out.println("Index: "+ i);
            // System.out.println(rows.size());

            // System.out.println(rows.get(i));
            String temp = rows.get(i);
            String[] line = temp.split(" ");

            switch(line[1]){
                case("->"):
                    // olika fall

                    // 1 -> y
                    if(isNumeric(line[0])){
                        valueMap.put(line[2], Integer.parseInt(line[0]));
                        rows.remove(i);
                        i = 0;
                    }
                    else {
                        // x -> y
                        if (valueMap.containsKey(line[0])) {
                            valueMap.put(line[2], valueMap.get(line[0]));
                            rows.remove(i);
                            i = 0;
                        }

                    }
                    break;
                    
                case("AND"):
                    // 1 AND 2
                    if (isNumeric(line[0]) && isNumeric(line[2])) {
                        valueMap.put(line[4], Integer.parseInt(line[0]) & Integer.parseInt(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    // 1 AND x
                    else if(isNumeric(line[0]) && valueMap.containsKey(line[2])) {
                        valueMap.put(line[4], Integer.parseInt(line[0]) & valueMap.get(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    // x AND 2
                    else if (valueMap.containsKey(line[0]) && isNumeric(line[2])) {
                        valueMap.put(line[4], valueMap.get(line[0]) & Integer.parseInt(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    // x AND y
                    else if(valueMap.containsKey(line[0]) && valueMap.containsKey(line[2])) {
                        valueMap.put(line[4], valueMap.get(line[0]) & valueMap.get(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    break; 
                    
                case("OR"):
                    // 1 OR 2
                    if (isNumeric(line[0]) && isNumeric(line[2])) {
                        valueMap.put(line[4], Integer.parseInt(line[0]) | Integer.parseInt(line[2]));
                        rows.remove(i);
                        i = 0;
                    } 
                    // 1 OR x
                    else if (isNumeric(line[0]) && valueMap.containsKey(line[2])) {
                        valueMap.put(line[4], Integer.parseInt(line[0]) | valueMap.get(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    // x OR 2
                    else if (valueMap.containsKey(line[0]) && isNumeric(line[2])) {
                        valueMap.put(line[4], valueMap.get(line[0]) | Integer.parseInt(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    // x OR y
                    else if (valueMap.containsKey(line[0]) && valueMap.containsKey(line[2])) {
                        valueMap.put(line[4], valueMap.get(line[0]) | valueMap.get(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    break;
                    
                case("LSHIFT"):
                    // 1 OR 2
                    if (isNumeric(line[0]) && isNumeric(line[2])) {
                        valueMap.put(line[4], Integer.parseInt(line[0]) << Integer.parseInt(line[2]));
                        rows.remove(i);
                        i = 0;
                    } 
                    // 1 OR x
                    else if (isNumeric(line[0]) && valueMap.containsKey(line[2])) {
                        valueMap.put(line[4], Integer.parseInt(line[0]) << valueMap.get(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    // x OR 2
                    else if (valueMap.containsKey(line[0]) && isNumeric(line[2])) {
                        valueMap.put(line[4], valueMap.get(line[0]) << Integer.parseInt(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    // x OR y
                    else if (valueMap.containsKey(line[0]) && valueMap.containsKey(line[2])) {
                        valueMap.put(line[4], valueMap.get(line[0]) << valueMap.get(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    break;
                    
                case("RSHIFT"):
                    // 1 OR 2
                    if (isNumeric(line[0]) && isNumeric(line[2])) {
                        valueMap.put(line[4], Integer.parseInt(line[0]) >> Integer.parseInt(line[2]));
                        rows.remove(i);
                        i = 0;
                    } 
                    // 1 OR x
                    else if (isNumeric(line[0]) && valueMap.containsKey(line[2])) {
                        valueMap.put(line[4], Integer.parseInt(line[0]) >> valueMap.get(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    // x OR 2
                    else if (valueMap.containsKey(line[0]) && isNumeric(line[2])) {
                        valueMap.put(line[4], valueMap.get(line[0]) >> Integer.parseInt(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    // x OR y
                    else if (valueMap.containsKey(line[0]) && valueMap.containsKey(line[2])) {
                        valueMap.put(line[4], valueMap.get(line[0]) >> valueMap.get(line[2]));
                        rows.remove(i);
                        i = 0;
                    }
                    break;
                    
                default: // NOT
                    // NOT 1
                    if (isNumeric(line[1])) {
                        valueMap.put(line[3], 65535 - Integer.parseInt(line[1]));
                        rows.remove(i);
                        i = 0;
                    }
                    // NOT x
                    else if (valueMap.containsKey(line[1])) {
                        valueMap.put(line[3], 65535 - valueMap.get(line[1]));
                        rows.remove(i);
                        i = 0;
                    }
                    break;
                    
            }
            if(i == rows.size()-1){
                i = 0;
            }
            else{
                i++;
            }
                
        }
        return valueMap.get("a");
    }

    public static void printMap(Map mp) {
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
            // it.remove(); // avoids a ConcurrentModificationException
        }
    }
    public static boolean isNumeric(String str) {  
        try {
            double d = Double.parseDouble(str);  
        }  
        catch(NumberFormatException nfe) {  
            return false;  
        }  
        return true;  
    }
}