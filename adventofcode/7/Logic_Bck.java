import java.io.*;
import java.util.*;

public class Logic2{
    private static Map<String, Integer> valueMap = new HashMap<String, Integer>(); // save variable data
    private static List<String> rows = new ArrayList<String>(); // save input file as rows

    public static void main(String[] args) throws FileNotFoundException{
        
        a = calculateA();
        

        printMap(valueMap);
        System.out.println("a = " + a);
        
    }
    public static int calculateA() {
        Scanner input = new Scanner(new File("test.txt"));
        String next;
        while(input.hasNextLine()) {
            next = input.nextLine();

            //System.out.println(next);
            rows.add(next); // saves file in arraylist
        }

        int i = 0;
        while(!rows.isEmpty()) {
            System.out.println("Index: "+ i);
            System.out.println(rows.size());

            System.out.println(rows.get(i));
            String temp = rows.get(i);
            String[] line = temp.split(" ");

            switch(line[1]){
                case("->"):
                    if(valueMap.containsKey(line[0])) {
                        valueMap.put(line[2], valueMap.get(line[0]));
                        rows.remove(i);
                        break;
                    }
                    else {
                        // om line[0] är string... break
                        if (isNumeric(line[0])) {
                            valueMap.put(line[2], Integer.parseInt(line[0]));
                            rows.remove(i);
                            break;
                        }
                        else{
                            break;
                        }
                        
                    }
                case("AND"):
                    // om x och y finns i hashmap -> värden finns, går å köra raden
                    if((valueMap.containsKey(line[0]) && valueMap.containsKey(line[2])) || (isNumeric(line[0]) && valueMap.containsKey(line[2])) ||(valueMap.containsKey(line[0]) && isNumeric(line[2]))) { 
                        
                        // bitwise and for x and y
                        int tmp = valueMap.get(line[0]) & valueMap.get(line[2]); 
                        valueMap.put(line[4], tmp);
                        rows.remove(i);

                        i = 0;
                        // // reset i?
                        break;
                    }
                    else{
                        break;
                    }
                case("OR"):
                    // om x och y finns i hashmap -> värden finns, går å köra raden
                    if(valueMap.containsKey(line[0]) && valueMap.containsKey(line[2])){
                        
                        // bitwise and for x or y
                        int tmp = valueMap.get(line[0]) | valueMap.get(line[2]); 
                        valueMap.put(line[4], tmp);
                        rows.remove(i);

                        i = 0;
                        // // reset i?
                        break;
                    }
                    else{
                        break;
                    }

                case("LSHIFT"):
                    // om x finns i hashmap -> värden finns, går å köra raden
                    if(valueMap.containsKey(line[0])) {
                        
                        // bitwise x << 2
                        int tmp = valueMap.get(line[0]) << 2;
                        valueMap.put(line[4], tmp);
                        rows.remove(i);

                        i = 0;
                        // // reset i?
                        break;
                    }
                    else{
                        break;
                    }
                case("RSHIFT"):
                    // om x finns i hashmap -> värden finns, går å köra raden
                    if(valueMap.containsKey(line[0])) {
                        
                        // bitwise x >> 2
                        int tmp = valueMap.get(line[0]) >> 2;
                        valueMap.put(line[4], tmp);
                        rows.remove(i);

                         i = 0;
                        // // reset i?
                        break;
                    }
                    else{
                        break;
                    }
                default:
                    if (line[0].equals("NOT")) {
                        if(valueMap.containsKey(line[1])) {
                            int tmp = 65535 - (valueMap.get(line[1]));
                            valueMap.put(line[3], tmp);
                            rows.remove(i);

                            i = 0;
                            // // reset i?
                            break;
                        }
                    }
                    else {
                        break;
                    }
            }
                
                if(i == rows.size()-2) {
                    i = 0;
                }
                else{
                    i++;
                }
        }
        return valueMap.get("a");
    }

    public static void printMap(Map mp) {
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
            // it.remove(); // avoids a ConcurrentModificationException
        }
    }
    public static boolean isNumeric(String str) {  
        try {
            double d = Double.parseDouble(str);  
        }  
        catch(NumberFormatException nfe) {  
            return false;  
        }  
        return true;  
    }
}