import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Strings2{
    private static int niceStrings = 0;

    public static void main(String args[]) throws FileNotFoundException{
        Scanner input = new Scanner(new File("input.txt"));
        String next;

        while(input.hasNextLine()){
            next = input.nextLine();
            if(checkString(next)){
                niceStrings++;
            }
        }
    
        System.out.println(niceStrings);

    }

    public static boolean checkString(String str){
        int len = str.length();
        String sub;
        boolean two = false;
        boolean repeat = false;
       // ta ut två tecken å kolla om de finns längre fram i strängen. loopa igenom strängen
        for(int i = 0; i < len - 3; i++){
            sub = str.substring(i,i+2);


            if(str.substring(i+2, len).contains(sub)){
                two =  true;
            }
        }
        for (int i = 0; i < len - 2; i++) {
            if(str.charAt(i)==str.charAt(i+2)){
                repeat = true;
            }
        }


        return (two && repeat);
    }
}