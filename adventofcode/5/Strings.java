import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Strings{
    private static int niceStrings = 0;
    private static String[] badStrings = {"ab", "cd", "pq", "xy"};
    private static String[] vowels = {"a", "e", "i", "o", "u"};

    public static void main(String args[]) throws FileNotFoundException{
        Scanner input = new Scanner(new File("input.txt"));
        String next;

        while(input.hasNextLine()){
            next = input.nextLine();
            if(checkString(next)){
                niceStrings++;
            }
        }
    
        System.out.println(niceStrings);

    }

    public static boolean checkString(String str){
        int vowel = 0;
        int twice = 0;
        char prev = ' ';

        // check for a bad string
        for(String s: badStrings){
            if(str.contains(s)){
                return false;
            }
        }

        
        for(char c: str.toCharArray()){

            // check if double chars
            if(c == prev){
                twice++;
            }
            // check for three vowels
            if(Arrays.asList(vowels).contains(Character.toString(c))) {
                vowel++;
           }
            prev = c;
        }

        return (vowel>2 && twice > 0);
    }
}