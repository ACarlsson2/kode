import java.util.*;
import java.io.*;

public class Houses2{
    private static int houses = 0;
    private static int sxPos = 0;
    private static int syPos = 0;
    private static int rxPos = 0;
    private static int ryPos = 0;
    private static Map<String, Integer> santaMap = new HashMap<String, Integer>();
    private static Map<String, Integer> roboMap = new HashMap<String, Integer>();
    private static String key;
    private static int santaORrobo = 0;

    public static void main(String arg[]) throws IOException {
        key = sxPos+","+syPos;
        checkSantaMap(key);
        
        int next;
        BufferedReader in;

        in = new BufferedReader(new FileReader("data3.txt"));
        next = in.read();

        while(next != -1){
            if(santaORrobo == 0){
                switch (next) {
                case 62: 
                    System.out.println("Santa Right");
                    sxPos++;
                    break;
                case 60: 
                    System.out.println("Santa Left");
                    sxPos--;
                    break;
                case 94: 
                    System.out.println("Santa Up");
                    syPos++;
                    break;
                case 118: 
                    System.out.println("Santa Down");
                    syPos--;
                    break;
                default: 
                    System.out.println("Another sign");
                    break;
                }
                key = sxPos+","+syPos;
                if(!roboMap.containsKey(key)){
                    checkSantaMap(key);
                    
                }
                next = in.read();
                santaORrobo++;
            }
            else {
                switch (next) {
                case 62: 
                    System.out.println("Robo Right");
                    rxPos++;
                    break;
                case 60: 
                    System.out.println("Robo Left");
                    rxPos--;
                    break;
                case 94: 
                    System.out.println("Robo Up");
                    ryPos++;
                    break;
                case 118: 
                    System.out.println("Robo Down");
                    ryPos--;
                    break;
                default: 
                    System.out.println("Another sign");
                    break;
                }
                key = rxPos+","+ryPos;
                if(!santaMap.containsKey(key)){
                    checkRoboMap(key);
                    
                }
                next = in.read();
                santaORrobo--;

            }
        }
        //System.out.printf("X and Y: %d %d\n", sxPos,syPos);
        System.out.printf("Amount of houses: %d\n", houses);
    }

    public static void checkSantaMap(String key){
        if(santaMap.containsKey(key)){
            santaMap.put(key, santaMap.get(key)+1);
        }
        else {
            santaMap.put(key, 1);
            houses++;
            
        }
    }

    public static void checkRoboMap(String key){
        if(roboMap.containsKey(key)){
            roboMap.put(key, roboMap.get(key)+1);
        }
        else {
            roboMap.put(key, 1);
            houses++;

            
        }
    }
}