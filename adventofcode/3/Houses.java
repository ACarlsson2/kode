import java.util.*;
import java.io.*;

public class Houses{
    private static int houses = 0;
    private static int xPos = 0;
    private static int yPos = 0;
    private static Map<String, Integer> myMap = new HashMap<String, Integer>();
    private static String key;

    public static void main(String arg[]) throws IOException {
        key = xPos+","+yPos;
        checkMap(key);

        int next;
        BufferedReader in;

        in = new BufferedReader(new FileReader("input.txt"));
        next = in.read();

        while(next != -1){
            switch (next) {
                case 62: 
                    System.out.println("Right");
                    xPos++;
                    break;
                case 60: 
                    System.out.println("Left");
                    xPos--;
                    break;
                case 94: 
                    System.out.println("Up");
                    yPos++;
                    break;
                case 118: 
                    System.out.println("Down");
                    yPos--;
                    break;
                default: 
                    System.out.println("Another sign");
                    break;
                }
            key = xPos+","+yPos;
            checkMap(key);
            next = in.read();
        }
        //System.out.printf("X and Y: %d %d\n", xPos,yPos);
        System.out.printf("Amout of houses: %d\n", houses);
    }

    public static void checkMap(String key){
        if(myMap.containsKey(key)){
            myMap.put(key, myMap.get(key)+1);
        }
        else {
            myMap.put(key, 1);
            houses++;
        }
    }
}