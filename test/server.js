var express = require("express");
var ejs = require("ejs");
var app = express();

var testObj = {name : "Carlsson"};
var users = [];

app.set("view-engine", "ejs");

app.get("/", function(req, res) {
  res.render("index.ejs", testObj);
});

app.get("/register", function(req, res) {
  var user = req.body;
  users.push({
    name: user.name,
    misc: user.misc
  })
  res.send("Great success!!!");
})

app.listen(3000, function() {
  console.log("Magic is happening on port 3000!")
});
