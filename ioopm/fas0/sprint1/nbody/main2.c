#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#ifdef ANIMATE
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define X_SIZE 800
#define Y_SIZE 800
#endif

#define prec float
#define PI 3.14159265359
#define G 0.00606

static prec gdt = 1.0;

typedef struct {
  prec x;
  prec y;
  prec mass;
  prec xVelocity;
  prec yVelocity;
  prec xForce;
  prec yForce;
} body;


static void update(body* a, prec dt) {
  prec Ax = a->xForce / a->mass;
  prec Ay = a->yForce / a->mass;
  
  a->xVelocity += a->xForce * dt;
  a->yVelocity += a->yForce * dt;

  a->x += a->xVelocity * dt + (Ax*dt*dt) / 2.0;
  a->y += a->yVelocity * dt + (Ay*dt*dt) / 2.0;
}


static void resetForce(body* b) {
  b->xForce = 0.0;
  b->yForce = 0.0;
} 

static void addForce(body* a, body* b) {
  prec r = sqrt( pow(((a->x) - (b->x)),2) + pow(((a->y) - (b->y)),2));
  prec v_riktning_x = ((b->x) - (a->x)) / r;
  prec v_riktning_y = ((b->y) - (a->y)) / r;
  prec Fx = ((a->mass * b->mass * G) / r) * v_riktning_x;
  prec Fy = ((a->mass * b->mass * G) / r) * v_riktning_y;
  
  a->xForce += Fx;
  a->yForce += Fy;

}

static prec newRand() {
  prec r = (prec)((prec)rand()/(prec)RAND_MAX);
  return r;
}


void init(int N, body* star) {
  for(int a = 0;a < N;a++) {
    star->x = (rand() % 100) + 350.0;
    star->y = (rand() % 100) + 350.0;
    star->mass = (rand() % 90) + 10.0;
    star->xForce = 0.0;
    star->yForce = 0.0;
    star->xVelocity = 0.0;
    star->yVelocity = 0.0;
    star++;
  }
}

/*void init(int N, body* star) {
  for(int a = 0;a < N;a++) {
    star[a].x = (rand() % 100) + 350.0;
    star[a].y = (rand() % 100) + 350.0;
    star[a].mass = (rand() % 90) + 10.0;
    star[a].xForce = 0.0;
    star[a].yForce = 0.0;
    star[a].xVelocity = 0.0;
    star[a].yVelocity = 0.0;
  }
}*/

static void updateForces(int N, body* star) {
  for(int i = 0;i < N;i++) {
    for(int j = 0;j < N;j++) {
      if(i != j) {
	addForce(&(star[i]), &(star[j]));
      }
    }
  }
}


#ifdef ANIMATE
static void copyToXBuffer(body* star, XPoint* points, int N) {
  for(int a = 0;a < N;a++) {
    points[a].x = star[a].x;
    points[a].y = star[a].y;
  }
}
#endif

int main(int argc, char* argv[]) {
  int N = 200;
  int iter = 3000;

  if(argc == 3)
    {
      N = atoi(argv[1]);
      iter = atoi(argv[2]);
    }
	
  body *stars = malloc(sizeof(body) * N);

  init(N, stars);

#ifdef ANIMATE
  XPoint* points = malloc(sizeof(XPoint)*N);
  Display* disp;
  Window window, rootwin;
  int screen;

  disp = XOpenDisplay(NULL);
  screen = DefaultScreen(disp);
  rootwin = RootWindow(disp,screen);
  window = XCreateSimpleWindow(disp,rootwin,
			       0,0,X_SIZE,Y_SIZE,1,0,0);
  GC gc = XCreateGC(disp, window, 0, 0);
  XSetForeground(disp, gc, WhitePixel(disp, screen));
  XSetBackground(disp, gc, BlackPixel(disp, screen));
  XMapWindow(disp,window);

  XClearWindow(disp,window);	
	
  copyToXBuffer(stars, points, N);
  XDrawPoints(disp, window, gc, points, N, 0);

  XFlush(disp);

#endif

  clock_t start = clock();
  for(int i = 0; i < iter; i++)
    {

      updateForces(N, stars);

      for(int i = 0; i < N; i++){
	update(&(stars[i]), 0.005);
	resetForce(&(stars[i]));
      }
	  
#ifdef ANIMATE
      copyToXBuffer(stars, points, N);
      XDrawPoints(disp, window, gc, points, N, CoordModeOrigin);
      XClearWindow(disp,window);	
#endif
    }
  clock_t stop = clock();
  float diff = (float)(stop - start)/CLOCKS_PER_SEC;
  printf("Total: %lf seconds\n",diff);
  printf("Bodies: %d\n",N);
  printf("Iterations: %d\n", iter);

#ifdef ANIMATE
  XCloseDisplay(disp);
#endif

  return 0;
}
