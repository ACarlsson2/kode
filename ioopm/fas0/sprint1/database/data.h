#ifndef data
#define data

typedef struct node *Node;									// the struct used to represent the database. Each node has a key, a value and a pointer to the next node in the database. 


void welcomescreen();										// Prins a simple welcome screen using ASCII characters
															// Does not return anything

void prntOptions ();										// Prints a list of options the user can choose from.
															// Does not return anything


void readline(char *dest, int n, FILE *source);				// Reads a string from a file source and saves the string with the maximum length n in a destination "dest"
															// Does not return anything. The funktions has side effects.

Node readFileToDatabase(char argv[]);						// Opens the file argv and creates nodes from the information in the file.
															// returns the database represented as nodes.

void queryDatabase(Node databaseList);						// Searches the databaseList containing nodes for a key the user gives the funktions. 
															// If the key exists then the funktion returns the value connected to the same node
															// Does not return anything

Node updateDatabase(Node databaseList);						// Upates the databaseList. The user gets to enter a key and if the key exists in databaseList 
															// then the user get to update the value of the node to something new
															// Returns an updated versions of the databaseList

Node insertToDatabase(Node databaseList);					// The user get to insert a new node in the databaseList. User chooses any name for the key and any value of the node.
															// Returns an updated version of the databaseList


Node deleteInDatabase(Node databaseList);					// The user get to delete a node in the databaseList. User enters a key and if the key is found
															// the node containing that key is deleted.
															// Returns an updated version of the databaseList.

void prntDatabase(Node databaseList);						// Prints the containing keys and values of each node in the databaseList.
															// Does not return anything.

#endif
