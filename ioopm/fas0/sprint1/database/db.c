#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "data.h"

int main(int argc, char *argv[]){
  if (argc < 2){
    puts("Usage: db [FILE]");                                                // Skyddar sig mot dålig indata.
    return -1;
  }

  welcomescreen();

  Node databaseList = readFileToDatabase(argv[1]);
  int choice = -1;

  while(choice != 0){  
    prntOptions();
    
    scanf("%d", &choice);
    while(getchar() != '\n'); // Clear stdin
    
    switch(choice){
    case 1: // queryDatabase
      queryDatabase(databaseList);
      break;

    case 2: // update
      databaseList = updateDatabase(databaseList);
      break;

    case 3: // Insert
     databaseList = insertToDatabase(databaseList);
     break;

    case 4: // Delete
      databaseList = deleteInDatabase(databaseList);
      break;

    case 5:  // Print database
      prntDatabase(databaseList);
      break;

    case 0:  // Exit
      puts("Good bye!");
      break;

        default: // Please try again                                                      // Skyddar sig mot dålig indata.
      puts("Could not parse choice! Please try again");
          }
    puts("");
  }
  return 0;
}
