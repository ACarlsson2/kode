#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "data.h"

struct node{                    // informationsgömmning
  char *key;
  char *value;
  struct node *next;
};

char buffer[128];
Node cursor;
int found = 0;

void readline(char *dest, int maxLen, FILE *source){
  fgets(dest, maxLen, source);                            // Skydd mot dålig indata.
  int len = strlen(dest);
  if(dest[len-1] == '\n')
    dest[len-1] = '\0';
}



Node readFileToDatabase(char argv[]){
  char *filename = argv;                              
  printf("Loading database \"%s\"...\n\n", filename);   
  FILE *database = fopen(filename, "r");               
  Node databaseList = NULL;                                  
  while(!(feof(database))){
    Node newNode = malloc(sizeof(struct node));
    readline(buffer, 128, database);
    newNode->key = malloc(strlen(buffer) + 1);
    strcpy(newNode->key, buffer);
    readline(buffer, 128, database);
    newNode->value = malloc(strlen(buffer) + 1);
    strcpy(newNode->value, buffer);
    newNode->next = databaseList;
    databaseList = newNode;
  }
  return databaseList;
}



void queryDatabase(Node databaseList){
  printf("Enter key: ");
  readline(buffer, 128, stdin);
  puts("Searching database...\n");
  found = 0;
  cursor = databaseList;
  while(!found && cursor != NULL){
    if(strcmp(buffer, cursor->key) == 0){
      puts("Found entry:");
      printf("key: %s\nvalue: %s\n", cursor->key, cursor->value);
      found = 1;
    }else{
      cursor = cursor->next;
    }
  }
  if(!found){                                                                   // minska skadan?
    printf("Could not find an entry matching key \"%s\"!\n", buffer);
  }
  return;
}

Node updateDatabase(Node databaseList){
  printf("Enter key: ");
  readline(buffer, 128, stdin);
  puts("Searching database...\n");
  found = 0;
  cursor = databaseList;
  while(!found && cursor != NULL){
    if(strcmp(buffer, cursor->key) == 0){
      puts("Matching entry found:");
      printf("key: %s\nvalue: %s\n\n", cursor->key, cursor->value);
      found = 1;
    }else{
      cursor = cursor->next;
    }
  }
  if(!found){
    printf("Could not find an entry matching key \"%s\"!\n", buffer);
  }else{
    printf("Enter new value: ");
    readline(buffer, 128, stdin);
    free(cursor->value);
    cursor->value = malloc(strlen(buffer) + 1);
    strcpy(cursor->value, buffer);
    puts("Value inserted successfully!");
  }
  return databaseList;
}

Node insertToDatabase(Node databaseList){
  
  printf("Enter key: ");
  readline(buffer, 128, stdin);
  puts("Searching database for duplicate keys...");
  found = 0;
  cursor = databaseList;
  while(!found && cursor != NULL){
    if(strcmp(buffer, cursor->key) == 0){
      printf("key \"%s\" already exists!\n", cursor->key);
      found = 1;
    }else{
      cursor = cursor->next;
    }
  }
  if(!found){ // Insert new node to the front of the databaseList
    puts("Key is unique!\n");
    Node newNode = malloc(sizeof(struct node));
    newNode->key = malloc(strlen(buffer) + 1);
    strcpy(newNode->key, buffer);
    printf("Enter value: ");
    readline(buffer, 128, stdin);
    newNode->value = malloc(strlen(buffer) + 1);
    strcpy(newNode->value, buffer);
    newNode->next = databaseList;
    databaseList = newNode;
    puts("");
    puts("Entry inserted successfully:");
    printf("key: %s\nvalue: %s\n", databaseList->key, databaseList->value);
  }
  return databaseList;
}


Node deleteInDatabase(Node databaseList){
  
  printf("Enter key: ");
  readline(buffer, 128, stdin);
  puts("Searching database...\n");
  found = 0;
  cursor = databaseList;
  Node prev = NULL;
  while(!found && cursor != NULL){
    if(strcmp(buffer, cursor->key) == 0){
      if(prev == NULL){ // Delete first node
	databaseList = cursor->next;
      }else{
	prev->next = cursor->next;
      }
      found = 1;
      printf("Deleted the following entry:\nkey: %s\nvalue: %s\n", cursor->key, cursor->value);
    }else{
      prev = cursor;
      cursor = cursor->next;
    }
  }
  if(!found){
    printf("Could not find an entry matching key \"%s\"!\n", buffer);
  }
  return databaseList;
}


void prntDatabase(Node databaseList){
  cursor = databaseList;
  while(cursor != NULL){
    puts(cursor->key);
    puts(cursor->value);
    cursor = cursor->next;
  }
  
}

