#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "tree.h"

char Vbuffer[128];
char Kbuffer[128];




bst_node new_bstree(){
  bst_node root = malloc(sizeof(bst_node));
  return root;
}


void readline(char *dest, int maxLen, FILE *source){
  fgets(dest, maxLen, source);
  int len = strlen(dest);
  if(dest[len-1] == '\n')
    dest[len-1] = '\0';
}


bst_node insertToDatabase(bst_node node, char *key, char *value)
{
  if (node == NULL){
    bst_node tmp;
    tmp = malloc(sizeof(bst_node));
    tmp->key = key;
    tmp->value = value;
    tmp->left_child = NULL;
    tmp->right_child = NULL;
    return tmp;
  }
  else {
    if (strcmp(key, node->key) < 0)
      insertToDatabase(node->left_child, key, value);
    else
      if (strcmp(key, node->key) > 0)
        insertToDatabase(node->right_child, key, value);
        else
          printf("Key alredy exists.");   
  }
  return node;
}


void queryDatabase(bst_node node)
{
  printf("Enter key: ");
  readline(Kbuffer, 128, stdin);
  puts("Searching database...\n");
  int found = 0;
  while(!found && node !=NULL){
    if (strcmp(Kbuffer, node->key) == 0){
      puts("Found entry:");
      printf("key: %s\nvalue: %s\n", node->key, node->value);
      found = 1; 
    }
    else
      if (strcmp(Kbuffer, node->key) < 0)
        {
          node = node->left_child; 
        }
      else
        node = node->right_child;
  }
  if (!found)
    {
      printf("Could not find an entry matching key \"%s\"!\n", Kbuffer);
    }
  return;
}

bst_node updateDatabase(bst_node node)
{
  printf("Enter key: ");
  readline(Kbuffer, 128, stdin);
  puts("Searching database...\n");
  int found = 0;
  
  while (!found && node != NULL){
    if (strcmp(Kbuffer, node->key) == 0){
      puts("Matching entry found:");
      found = 1;
    }
    else
      {
        if (strcmp(Kbuffer, node->key) < 0)
          node = node->left_child;
        else 
          node = node->right_child;
    }
  }
  if (!found)
    {
      printf("Could not find an entry matching key \"%s\"!\n", Kbuffer);
    }
  else
    {
      printf("Key: %s \nValue: %s\n\n", node->key, node->value);
      printf("Enter new value: ");
      readline(Vbuffer, 128, stdin);
      free(node->value);
      node->value = malloc(strlen(Vbuffer) + 1);
      strcpy(node->value, Vbuffer);
      puts("Value inserted successfully!"); 
    }
  return node;
}
/*
Node deleteInDatabase(Node *node)
{
  
} 
*/
bst_node readFileToDatabase(char argv[]){
  char *filename = argv;                              
  printf("Loading database \"%s\"...\n\n", filename);   
  FILE *database = fopen(filename, "r");               
  bst_node root = new_bstree();                                  
  while(!(feof(database))){
    
    readline(Kbuffer, 128, database);
    readline(Vbuffer, 128, database);
    root = insertToDatabase(root, Kbuffer, Vbuffer);
  }
  return root;
}
