#ifndef _tree_h
#define _tree_h


typedef struct bst_node {
  char* key;
  char* value;
  struct bst_node *left_child;
  struct bst_node *right_child;
}bst_node;


void welcomescreen();       								// Prints a  simple welcome screen using ASCII-characters. 
															// Does not return anything.

void prntOptions ();										// Prtints a list of options the user get to choose from
															// Does not return anything

bst_node new_bstree();		//																				
void readline(char *dest, int n, FILE *source);  //            
bst_node readFileToDatabase(char argv[]);            //      
void queryDatabase(bst_node root);                    //  
bst_node updateDatabase(bst_node root);                   //      
bst_node insertToDatabase(bst_node root, char *key, char *value); // 
//Node deleteInDatabase(Node root);
//void prntDatabase(Node *root);




#endif
