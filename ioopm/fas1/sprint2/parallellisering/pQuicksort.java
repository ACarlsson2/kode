import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

class globals{
	static ForkJoinPool fjpool = new ForkJoinPool();
}

public class pQuicksort extends RecursiveAction{

int start;
int end;
int[] array;

public pQuicksort(int[] arr, int _start, int _end){

		start = _start;
		end = _end;
		array = arr;
}


protected void compute(){
		

 		int left = start;
    	int right = end + 1;
    	
    	final int pivot = arr[start];
    	int tmp;
    	
    	// Rearranging the elements around the pivot, so that
    	// elements smaller than the pivot end up to the left
    	// and elements bigger than the pivot end up to the
    	// right.
    	if(end - start <= 1024){
    	Quicksort.insertionSort(arr, start, end);
    	}
    	else{
    	do {
    		
    	    // As long as elements to the left are less than
    	    // the pivot we just continue.
    	    do {
    		left++;
    	    } while (left <= end && arr[left] < pivot);
    	    
    	    // As long as the elements to the right are
    	    // greater than the pivot we just continue.
    	    do {
    		right--;
    	    } while (arr[right] > pivot);
    	    
    	    // If left is less than right we have values on
    	    // the wrong side of the pivot, so we swap them.
    	    if (left < right) {
    		tmp = arr[left];
    		arr[left] = arr[right];
    		arr[right] = tmp;
    	    }
    	    
    	    // We continue doing this until all elements are
    	    // arranged correctly around the pivot.
    	} while (left <= right);
    	
    	// Now put the pivot in the right place.
    	tmp = arr[start];
    	arr[start] = arr[right];
    	arr[right] = tmp;
    
		
		pQuicksort left = new pQuicksort(array, start, right);
		pQuicksort right = new pQuicksort(array, left, end);
		left.fork;
		right.compute();
		left.join();
		return
		}		

	}		



	static void parallellsort(int[] arr){
		globals.fjpool.invoke(new pQuicksort(arr, 0, arr.length));

	}	
}	
/*

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

class Globals {
    static ForkJoinPool fjPool = new ForkJoinPool();
}

class Sum extends RecursiveTask<Long> {
    static final int SEQUENTIAL_THRESHOLD = 5000;

    int low;
    int high;
    int[] array;

    Sum(int[] arr, int lo, int hi) {
        array = arr;
        low   = lo;
        high  = hi;
    }

    protected Long compute() {
        if(high - low <= SEQUENTIAL_THRESHOLD) {
            long sum = 0;
            for(int i=low; i < high; ++i) 
                sum += array[i];
            return sum;
         } else {
            int mid = low + (high - low) / 2;
            Sum left  = new Sum(array, low, mid);
            Sum right = new Sum(array, mid, high);
            left.fork();
            long rightAns = right.compute();
            long leftAns  = left.join();
            return leftAns + rightAns;
         }
     }

     static long sumArray(int[] array) {
         return Globals.fjPool.invoke(new Sum(array,0,array.length));
     }
}*/