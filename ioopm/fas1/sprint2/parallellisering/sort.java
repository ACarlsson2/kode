import java.util.Arrays;
import java.util.Random;

public class sort {

	/**
	 * @param args
	 */
	
    public static boolean control(int[] arr){
    	boolean sorted = true;
    	
    	for	(int i = 0; i < arr.length -1; i++){
    		if(arr[i] > arr[i+1]){
    			sorted = false;
    		}
    	}
    	return sorted;
    }
  
    public static int[] createarr(){
    	Random rn = new Random();
    	int antal = rn.nextInt(999) + 1;
    	int[] arr = new int[antal];
    	
    	for(int i = 0; i < antal - 1; i++){
    		arr[i] = rn.nextInt(1000);
    	}
    	return arr;
    }
	
    public static int[] createarr(int antal){
    	Random rn = new Random();
    	int[] arr = new int[antal];
    	
    	for(int i = 0; i < antal - 1; i++){
    		arr[i] = rn.nextInt(1000);
    	}
    	return arr;
    }
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		static ForkJoinPool fjPool = new ForkJoinPool();  
		int[] test = createarr(2000);
    	int[] rndtest = createarr();
    	
    	
    	System.out.println(control(test));
    	long time = System.nanoTime();
    	Quicksort.sQsort(test);
    	time = System.nanoTime() - time;
    	System.out.println(time);
    	System.out.println(Arrays.toString(test));
    	System.out.println(control(test));
    	
    	System.out.println(control(rndtest));
    	time = System.nanoTime();
    	Quicksort.sQsort(rndtest);
    	time = System.nanoTime() - time;
    	System.out.println(time);
    	System.out.println(control(rndtest));


    	

	}

}
