
public class TrafficSystem {
    // Definierar de vägar och signaler som ingår i det 
    // system som skall studeras.
    // Samlar statistik
    
    // Attribut som beskriver beståndsdelarna i systemet
    private Lane  r0;
    private Lane  r1;
    private Lane  r2;
    private Light s1;
    private Light s2;

    private int arrivalInt;
    private int d2Int;
    private int greenTime1;
    private int greenTime2;
    private int periodTime;
    private int r0Length;
    private int r12Length;

    
    // Diverse attribut för simuleringsparametrar (ankomstintensiteter,
    // destinationer...)

    // Diverse attribut för statistiksamling
 
    
    private int time = 0;

    public TrafficSystem() {
	this.readParameters();
	this.r0 = new Lane(r0Length);
	this.r1 = new Lane(r12Length);
	this.r2 = new Lane(r12Length);
	this.s1 = new Light(periodTime, greenTime1);
	this.s2 = new Light(periodTime, greenTime2);
	
}

    public void readParameters() {
	this.arrivalInt = 50;
	this.d2Int = 25;
	this.greenTime1 = 8;
	this.greenTime2 = 5;
	this.periodTime = 15;
	this.r0Length = 10;
	this.r12Length = 5;
	 
	// Läser in parametrar för simuleringen
	// Metoden kan läsa från terminalfönster, dialogrutor
	// eller från en parameterfil. Det sista alternativet
	// är att föredra vid uttestning av programmet eftersom
	// man inte då behöver mata in värdena vid varje körning.
        // Standardklassen Properties är användbar för detta. 
    }

    public void step() {
	
	if (this.s1.isGreen()){                      // Step r1
	    Car out1 = this.r1.getFirst();	    
	}
	
	if (this.s2.isGreen()){                       // Step r2
	    Car out2 = this.r2.getFirst();
	}
	
	this.r1.step();
	this.r2.step();
	
	if (this.r0.firstCar() != null){              // Step r0
                                                  
	    Car temp = this.r0.firstCar();            // Finns bil
	    if (temp.getdest() == 1 && this.r1.lastFree()) {
		this.r1.putLast(this.r0.getFirst());
	    }                  
	    else {
		if (this.r2.lastFree()){
		    this.r2.putLast(this.r0.getFirst());
		}
	    }
	}
	
	this.r0.step();
	this.s1.step();
	this.s2.step();
	




	if(arrivalInt <= (Math.random()*100)){
	   Car ake = new Car(time);
	   this.r0.putLast(ake);
	}
    }

	// Stega systemet ett tidssteg m h a komponenternas step-metoder
	// Skapa bilar, lägg in och ta ur på de olika Lane-kompenenterna
	   /*

    public void printStatistics() {
	// Skriv statistiken samlad så här långt
    }

    public void print() {
	// Skriv ut en grafisk representation av kösituationen
	// med hjälp av klassernas toString-metoder
    }
	   */
}
	   
