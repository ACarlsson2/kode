import java.util.Scanner;

public class Die {
    private int numberOfSides;
    private int value;
    
    public String toString(){
    	return "Die(" + value + ")";
    }
    
    
    public boolean equals(Die otherDie){
	if (otherDie.value ==  this.value) {
	    return true;
	}
	else
	    return false;
    } 
    
    /**
     * Här har vi die-konstruktorn
     */
    public Die() { 
      numberOfSides = 6; 
    }
    
    public Die(int _numberOfSides) { 
	if (_numberOfSides > 0){
	    numberOfSides = _numberOfSides;
	}
	else{
	    System.out.println("fuck off");
	    System.exit(0);
	}      
    }
    
    public int roll() {
	return value =  (int) (Math.random()*numberOfSides) + 1;
    }
    
    public int get() { 
	int x = 0;
	int sum = 0;
	Scanner sc = new Scanner(System.in);
	System.out.print("Number of sides: ");
	Die d = new Die(sc.nextInt());
	// System.out.println("Alea iacta est: " + d.roll());
	while (x < 10){
	    int r = d.roll();
	    System.out.println("Roll" + x +"=" + r);
	    sum += r;
	    x++;
	}
	
	return sum; 
    }

    public static void main(String [] args) {
	Scanner sc = new Scanner(System.in);
	System.out.print("Number of sides: ");
	Die d = new Die(sc.nextInt());
	System.out.println("Alea iacta est: " + d.roll());
    } 
}
