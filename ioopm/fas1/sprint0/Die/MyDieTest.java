import java.util.Scanner;

public class MyDieTest {

    public static void main(String [] args){
	int x = 0;
	int sum = 0;
	Scanner sc = new Scanner(System.in);
	System.out.print("Number of sides: ");
	Die d = new Die(sc.nextInt());
	
	while(x < 10) {
	    int r = d.roll();
	    System.out.println("Roll" + x +"=" + r);
	    sum += r;
	    x++;
	}
	System.out.println("Alea iacta est: " + sum);
    }
    
}
