import os
import swiftclient.client
import paramiko
import time

def create_worker(workers):

    # config = {'username':os.environ['OS_USERNAME'], 
    #           'api_key':os.environ['OS_PASSWORD'],
    #           'project_id':os.environ['OS_TENANT_NAME'],
    #           'auth_url':os.environ['OS_AUTH_URL'],
    #            }
    config = {'username':"anca9260", 
              'api_key':"anton",
              'project_id':"ACC-Course",
              'auth_url':"http://smog.uppmax.uu.se:5000/v2.0",
               }
    from novaclient.client import Client
    nc = Client('2',**config)



    workerIPS = []

    for n in range(0,workers):
        print "Finding keypair..."
        if not nc.keypairs.findall(name="projectKey"):
            with open(os.path.expanduser('cloud.pub')) as fpubkey:
                nc.keypairs.create(name="projectKey", public_key=fpubkey.read())       
        keypair = nc.keypairs.find(name="projectKey")

        nc.images.list()
        #image = nc.images.find(name="Ubuntu Server 14.04 LTS (Trusty Tahr)")
        image = nc.images.find(name="carlsson_worker")
        flavor = nc.flavors.find(name="m1.medium")

        nc.networks.list()
        network = nc.networks.find(label="ACC-Course-net")

        print "Getting userdata..."
        userData = open("userdata_worker.yml","r")

        print "Creating server..."
        server = nc.servers.create(name = "Carlssons_Worker"+str(n), image = image, flavor = flavor, network = network, key_name = keypair.name, userdata=userData)

        status = server.status
        while status == 'BUILD':
            time.sleep(5)
            # Retrieve the instance again so the status field updates
            instance = nc.servers.get(server.id)
            status = instance.status
        print "status: %s" % status

        iplist = nc.floating_ips.list()
        for ip_obj in iplist:
            if ((getattr(ip_obj,'instance_id')) == None):
                floating_ip = getattr(ip_obj, 'ip')
                workerIPS.append(floating_ip)
                break
        else:
            print "No IP:s available!"

        print "Attaching IP: " + floating_ip
        
        server.add_floating_ip(floating_ip)

        nc.security_groups.list()
        secgroup = nc.security_groups.find(name="CarlssonSecurity")
        
    print workerIPS


    # wait_time = 80
    # for i in range(0,8):
    #     time.sleep(10)
    #     wait_time -= 10
    #     print str(wait_time)+"s remaining..."

    # for ip in workerIPS:
    #     ssh = paramiko.SSHClient()
    #     ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    #     cmd = "cd /home/ubuntu/;git clone https://ACarlsson2@bitbucket.org/ACarlsson2/project.git"
    #     cmd2 = "cd /home/ubuntu/project;celery worker -A task &"
    #     try:
    #         ssh.connect(ip, username='ubuntu', password = None, key_filename="cloud")
    #         print "CONNECTED!"
    #         stdin, stdout, stderr = ssh.exec_command(cmd)
    #         print "running cmd " + str(cmd)
    #         time.sleep(10)
    #         stdin, stdout, stderr = ssh.exec_command(cmd2)
    #         print "running cmd " + str(cmd2)
    #     except Exception as e:
    #         print e
    #         print "ERRROR SSHING!!!!!!"
    #     ssh.close()

