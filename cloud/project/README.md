To run the program: 

Start a broker by running "create_broker.py".
The script prints the IP for the broker created which has to be added to "task.py".

Edit the BROKERIP to the new IP address and push the "task.py" file to the repo.

Go to *brokerip*:5000 in a brower to run.