import time
import os
#import paramiko
from celery import group, subtask
from flask import Flask
from flask import jsonify
from flask import render_template
from flask import request, redirect
from task import work
from collections import Counter
from cStringIO import StringIO
import urllib2
import subprocess
from subprocess import call
from novaclient.client import Client
from create_worker import create_worker


app = Flask(__name__)

@app.route('/')
def index():
    os.environ['LC_ALL'] = "en_US.utf8"
    return render_template('index.html')

def calc(angle_start, k, i):
    angle_start = int(angle_start)
    k = int(k)

    return angle_start+(i*k)

@app.route('/submit', methods=['POST'])
def create_work():
    # config = {'username':os.environ['OS_USERNAME'], 
    #           'api_key':os.environ['OS_PASSWORD'],
    #           'project_id':os.environ['OS_TENANT_NAME'],
    #           'auth_url':os.environ['OS_AUTH_URL'],
    #            }
    config = {'username':"anca9260", 
              'api_key':"anton",
              'project_id':"ACC-Course",
              'auth_url':"http://smog.uppmax.uu.se:5000/v2.0",
               }
    nc = Client('2',**config)

    angle_start = request.form['angle_start']
    angle_stop = request.form['angle_stop']
    n_angles = request.form['n_angles']
    n_nodes = request.form['n_nodes']
    n_levels = request.form['n_levels']

    # num_samples = request.form['num_samples']
    # visc = request.form['visc']
    # speed = request.form['speed']
    # time = request.form['time']

    k = (int(angle_stop)-int(angle_start))/int(n_angles)
    commands = []
    for i in range(0,int(n_angles)):
        toRun = str(calc(angle_start, k, i)) + ' ' + str(calc(angle_start, k, i)) + ' ' + str(1) + ' ' + n_nodes + ' ' + str(0)

        print toRun
        commands.append(toRun)

    serverlist = nc.servers.findall()
    runningServers = 0
    for server in serverlist:
        if(server.name.startswith("Carlssons_Worker")):
            print "Found worker: " + str(server.name)
            runningServers += 1

    print "Running workers: " + str(runningServers)
    if (runningServers < 2):
        if (len(n_angles)<10):
            print "Starting 2 workers"
            create_worker(2)
        else:
            print "Starting 4 workers"
            create_worker(4)

    worker_tasks = []
    for command in commands:
        worker_tasks.append(work.s(command))

    tasks = group(worker_tasks)
    res = tasks.apply_async()


    while (res.ready() == False):
        pass

    ratios = res.get()



    serverlist = nc.servers.findall()
    for server in serverlist:
        if(server.name.startswith("Carlssons_Worker")):
          print "Terminating instance: " + server.name
          server.delete()

    return render_template('result.html', data=ratios)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
