from __future__ import division
import json
import os
from celery import Celery
import urllib2
from cStringIO import StringIO
import subprocess
from subprocess import call
import time



brokerIP = "carlsson:carlsson@130.238.29.6/carlssonvhost"

celery = Celery('parse', backend='amqp', broker='amqp://' + brokerIP)

def calcforce():
    f = open("/home/ubuntu/project/results/drag_ligt.m","r")
    #file = f.readlines()
    next(f)
    lift = 0.0
    drag = 0.0

    for line in f:
        text = line.split('\t')
        print line
        print text
        print text[1]
        print text[2]
        lift += float(text[1])
        drag += float(text[2])

    #liftdrag = lift/drag
    f.close()
    return [lift, drag]

@celery.task()
def work(params):
    os.environ['LC_ALL'] = "en_US.utf-8"
    print "Running gmsh on: " + params
    toRun = 'sudo ./run.sh ' + params
    subprocess.call(toRun, shell=True)

    for fn in os.listdir('/home/ubuntu/project/msh'):
        if (fn.endswith(".msh")):
            command = "sudo chmod 777 /home/ubuntu/project/msh/" + fn
            subprocess.call(command, shell=True)
            print str(fn)
            name = "sudo dolfin-convert " + "/home/ubuntu/project/msh/" + fn + " " + "/home/ubuntu/project/msh/" + fn[:-4] + ".xml"
            print name
            subprocess.call(name, shell=True)

    files = os.listdir('/home/ubuntu/project/msh')
    xmlfiles = []
    for file in files:
        if (file.endswith('.xml')):
            command = "sudo chmod 777 /home/ubuntu/project/msh/" + file
            subprocess.call(command, shell=True)
            xmlfiles.append(file)

    lift = 0
    drag = 0
    for file in xmlfiles:
        toRun = 'sudo ./navier_stokes_solver/airfoil' + ' ' + str(10) + ' ' + str(0.0001) + ' ' + str(10) + ' ' + str(1) + ' ' + '/home/ubuntu/project/msh/' + file
        print toRun
        subprocess.call(toRun.split(" "))
        liftdrag = calcforce()
        lift +=  liftdrag[0]
        drag += liftdrag[1]


    # for mfiles in (os.listdir("/home/ubuntu/project/results")):
    #     if (mfiles.endswith(".m")):
    #         #os.rename(mfiles, file[:-4] + ".m")
    


    # ratio = []
    # for file in os.listdir("/home/ubuntu/project/results"):
    #     if (file.endswith(".m")):
    #         ratio.append(calcforce(file))
    
    # clean up folders
    cleanmsh = "sudo rm /home/ubuntu/project/msh/*"
    cleangeo = "sudo rm /home/ubuntu/project/geo/*"
    subprocess.call(cleanmsh, shell=True)
    subprocess.call(cleangeo, shell=True)

    return "Angle: " + str(params.split(' ')[0]) + ". Lift: " + str(lift) + ". Drag: " + str(drag)
