import os
import swiftclient.client
import paramiko
import time

# config = {'user':os.environ['OS_USERNAME'], 
#           'key':os.environ['OS_PASSWORD'],
#           'tenant_name':os.environ['OS_TENANT_NAME'],
#           'authurl':os.environ['OS_AUTH_URL']}

# conn = swiftclient.client.Connection(auth_version=2, **config)
# import uuid
# bucket_name = "CarlssonTweet".format(str(uuid.uuid4()))
# conn.put_container(bucket_name)

# tweetCont = conn.get_container(bucket_name)
# for x in range (len(tweetCont[1])):
#     print CarlssonCont[1][x]["name"]

def create_broker():

    # config = {'username':os.environ['OS_USERNAME'], 
    #           'api_key':os.environ['OS_PASSWORD'],
    #           'project_id':os.environ['OS_TENANT_NAME'],
    #           'auth_url':os.environ['OS_AUTH_URL'],
    #            }
    config = {'username':"anca9260", 
              'api_key':"anton",
              'project_id':"ACC-Course",
              'auth_url':"http://smog.uppmax.uu.se:5000/v2.0",
               }
    from novaclient.client import Client
    nc = Client('2',**config)

    # Use paramiko to access your instance and, using ssh, start the cowsay service on your instance,
    # usinf the same command as in Task 4, lab 1. 
    

    print "Finding keypair..."
    if not nc.keypairs.findall(name="projectKey"):
        with open(os.path.expanduser('cloud.pub')) as fpubkey:
            nc.keypairs.create(name="projectKey", public_key=fpubkey.read())       
    keypair = nc.keypairs.find(name="projectKey")

    nc.images.list()
    image = nc.images.find(name="Ubuntu Server 14.04 LTS (Trusty Tahr)")
    flavor = nc.flavors.find(name="m1.medium")

    nc.networks.list()
    network = nc.networks.find(label="ACC-Course-net")

    print "Getting userdata..."
    userData = open("userdata_broker.yml","r")

    print "Creating server..."
    server = nc.servers.create(name = "Carlssons_Broker", image = image, flavor = flavor, network = network, key_name = keypair.name, userdata=userData)

    status = server.status
    while status == 'BUILD':
        time.sleep(5)
        # Retrieve the instance again so the status field updates
        instance = nc.servers.get(server.id)
        status = instance.status
    print "status: %s" % status

    iplist = nc.floating_ips.list()
    for ip_obj in iplist:
        if ((getattr(ip_obj,'instance_id')) == None):
            floating_ip = getattr(ip_obj, 'ip')
            break
    else:
        print "No IP:s available!"

    print "Attaching IP:"
    print floating_ip
    server.add_floating_ip(floating_ip)

    nc.security_groups.list()
    secgroup = nc.security_groups.find(name="CarlssonSecurity")
    
    return floating_ip

create_broker()
