import time
from parse import parse
from celery import group, subtask
from flask import jsonify
from flask import render_template
from flask import request, redirect
from collections import Counter
from config import app



@app.route('/', methods=['GET'])
def create_worker():

    queue = [parse.s('tweets_{}.txt'.format(x)) for x in xrange(0,20)]
    g = group(queue)
    res = g()

    while (res.ready() == False):
        time.sleep(3)

    dicts = res.get()
    counter = Counter()
    for dic in dicts:
        counter.update(dic)

    data = dict(counter)
    return render_template('result.html', data=data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)