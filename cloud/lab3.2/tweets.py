import time
from celery import group, subtask
from flask import jsonify
from flask import render_template
from flask import request, redirect
from parse import parse
from collections import Counter
from config import app
from cStringIO import StringIO
import urllib2



@app.route('/', methods=['GET'])
def create_work():

    # queue = [parse.s('tweets_{}.txt'.format(x)) for x in xrange(0,20)]
    # g = group(queue)
    # res = g()

    req = urllib2.Request("http://smog.uppmax.uu.se:8080/swift/v1/tweets")
    response = urllib2.urlopen(req)
    names = response.read().split()


    worker_tasks = []
    for name in names:
        worker_tasks.append(parse.s(name))

    # Start workers
    tasks = group(worker_tasks)
    res = tasks.apply_async()


    while (res.ready() == False):
        time.sleep(3)

    dicts = res.get()
    counter = Counter()
    for dic in dicts:
        counter.update(dic)

    data = dict(counter)
    return render_template('result.html', data=data)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)