import json
from celery import Celery
import urllib2
from cStringIO import StringIO


brokerIP = "carlsson:carlsson@130.238.29.137/carlssonvhost"

celery = Celery('parse', backend='amqp', broker='amqp://' + brokerIP)

def counter(word, dic):
    keys = dic.keys()
    for key in keys:
        if word == key:
            dic[key] +=1

@celery.task()
def parse(filename):
    dic = {'han': 0, 'hon': 0, 'den': 0, 'det': 0,
            'denna': 0, 'denne': 0, 'hen': 0, 'total_tweets': 0}

    req = urllib2.Request("http://smog.uppmax.uu.se:8080/swift/v1/tweets/" + filename)
    response = urllib2.urlopen(req)
    obj = response.read()

    txtfile = StringIO(obj)
    for row in txtfile:
        if row == "\n":
            pass
        else:
            json_row = json.loads(row)
            if 'retweeted_status' not in json_row:
                dic['total_tweets'] +=1
                words = json_row['text'].split()
                for word in words:
                    counter(word.lower(), dic)
    return dic