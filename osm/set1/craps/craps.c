/**
 * Game of luck: Implementation of the Gamemaster
 *
 * Course: Operating Systems and Multicore Programming - OSM lab
 * assignment 1: game of luck.
 *
 * Author: Nikos Nikoleris <nikos.nikoleris@it.uu.se>
 *
 */

#include <stdio.h> /* I/O functions: printf() ... */
#include <stdlib.h> /* rand(), srand() */
#include <unistd.h> /* read(), write() calls */
#include <assert.h> /* assert() */
#include <time.h>   /* time() */
#include <signal.h> /* kill(), raise() and SIG???? */

#include <sys/types.h> /* pid */
#include <sys/wait.h> /* waitpid() */

#include "common.h"

int main(int argc, char *argv[])
{
  int i, seed, score, player;
  int leader = 0;
  pid_t pid; //process id
  int seedP [NUM_PLAYERS][2];
  int scoreP [NUM_PLAYERS][2];
  pid_t allChildren[NUM_PLAYERS];

  /* TODO: Use the following variables in the exec system call. Using the
   * function sprintf and the arg1 variable you can pass the id parameter
   * to the children
   */

  char arg0[] = "./shooter";
  char arg1[10];
  char *args[] = {arg0, arg1, NULL};
  /* TODO: initialize the communication with the players */
  for (i = 0; i < NUM_PLAYERS; i++) {
    pipe(seedP[i]); //Creating pips
    pipe(scoreP[i]);
  }

  for (i = 0; i < NUM_PLAYERS; i++) {
    /* TODO: spawn the processes that simulate the players */
	  
    switch(pid = fork()) {
    case -1: 
      perror("parent couldn't create a child!");
      exit(EXIT_FAILURE);
    case 0:
      dup2(seedP[i][0], STDIN_FILENO);    //slutet p� r�ret 0 (read) ska till andra programmet som vars del blir STDIN_FILENO  allts� input
      dup2(scoreP[i][1], STDOUT_FILENO);
      //shooter(i, seedP[i][0], scoreP[i][1]); //seedP[i][0]   0 f�r att l�sa 1 f�r att skriva fr�n r�r i (2 r�r)
      sprintf(arg1,"%d",i);
      execv(arg0, args); //Kommer aldrig tebaks efter den bytt program
    default:
	    
      allChildren[i] = pid;
      break;   //kan vara bra att h�lla koll p� farsan?
    }
  }

  seed = time(NULL);
  for (i = 0; i < NUM_PLAYERS; i++) {
    seed++;
    /* TODO: send the seed to the players */
	  
    if(~1 == (write(seedP[i][1], &seed, sizeof(int)))) {  //if write fails return -1
      perror("failed to write!");
      exit(EXIT_FAILURE);
    }
  }
	
  /* TODO: get the dice results from the players, find the winner */
  for (i = 0; i < NUM_PLAYERS; i++) {
    read(scoreP[i][0],&score,sizeof(int));
    if(score > leader) {
      leader = score;
      player = i;
    }
	  
  }
  printf("master: player %d WINS\n", player);

  /* TODO: signal the winner */
	

  /* TODO: signal all players the end of game */
  for (i = 0; i < NUM_PLAYERS; i++) {
    if(player == i) {
      kill(allChildren[i], SIGUSR1);
    }
    kill(allChildren[i], SIGUSR2);
  }
	
  for (i = 0; i < NUM_PLAYERS; i++) {
    wait(NULL); //Will kill all zombies
  }
  printf("master: the game ends\n");

  /* TODO: cleanup resources and exit with success */
  for (i = 0; i < NUM_PLAYERS; i++) {
    close(scoreP[i]);
    close(seedP[i]);
  }
  exit(EXIT_SUCCESS);
  return 0;
}
