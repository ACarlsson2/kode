%% @doc Erlang mini project.
-module(add).
-export([start/3, start/4, makeList/1, makeList/2,fillaux/2, split/2]).

-include_lib("eunit/include/eunit.hrl").

%% @doc TODO: add documentation
-spec start(A,B,Base) -> ok when 
      A::integer(),
      B::integer(), 
      Base::integer().

start(A,B, Base) ->
    start(A, B, Base, [1]).

%% @doc TODO: add documentation
-spec start(A,B,Base, Options) -> ok when 
      A::integer(),
      B::integer(), 
      Base::integer(),
      Option::atom() | tuple(),
      Options::[Option].

start(A,B,Base, Options) ->
   tbi.


%% @doc split the list L in to N segements of the same length
-spec split(L,N) -> ok when
  L::[integer()],
  N::integer().

  split(L,N) -> 
  S = length(L) div N,
  R = length(L) rem N,
  splitaux(L,N,S,R,[]).

-spec splitaux(L1,N, S, R,L2) -> ok when
L1::[integer()],
N::integer(),
S::integer(),
R::integer(),
L2::[integer()].

splitaux([],_,_,_,ListOut) -> lists:reverse(ListOut);

splitaux(ListIn, N, S, 0, ListOut)->
    {List2, List3} = lists:split(S, ListIn),
    splitaux(List3,N,S,0,[List2|ListOut]);

splitaux(ListIn, N, S, R, ListOut) ->
NewL = fillaux(length(ListIn)+(N-R),ListIn),
NewS = length(NewL) div N,
splitaux(NewL, N, NewS, 0, ListOut).

%% @doc extends the shortest list with zeros.
-spec fill({A,B}) -> ok when
A::[integer()],
B::[integer()].

fill({A,B}) -> 
MaxL = max(length(A),length(B)),
{fillaux(MaxL,A),fillaux(MaxL,B)}.


%% Help function for fill
-spec fillaux(M,L) -> ok when
M::integer(),
L::[integer()].

fillaux(M,L) ->
case length(L) of
M -> L;
_ -> fillaux(M, [0|L])
end.



%% @doc transforms I into a int list.
-spec makeList(I) -> ok when
  I::integer().

makeList(I) -> makeList(I,[]).

-spec makeList(I, L) -> ok when
  I::integer(),
  L::[integer()].

makeList(I,L) ->
case I of
  0 -> L;
  _ -> R = I rem 10,
  List = [R|L],
  NewI = (I-R) div 10,
  makeList(NewI,List)
  end.



%%=====================================================================
%%    TEST CASES!
%%=====================================================================

  makeList_test_() ->
  [?_assertEqual([1,5,0], makeList(150))],
  [?_assertEqual([1,0,1], makeList(101))].

fill_test_() ->
[?_assertEqual({[1,5],[0,5]}, fill({[1,5],[5]}))],
[?_assertEqual({[],[]}, fill({[],[]}))],
[?_assertEqual({[1,5],[2,5]}, fill({[1,5],[2,5]}))].

split_test_() ->
[?_assertEqual([[1,2],[3,4],[5,6]],split([1,2,3,4,5,6],3))],
[?_assertEqual([[0,1],[2,4],[3,5],[7,6]],split([1,2,4,3,5,7,6],4))],
[?_assertEqual([[0,3],[3,3]],split([3,3,3],2))].



%    Saker att skriva.
%    
%    Dela upp listor
%
%
%
%
% 1. Gör om till listor
% 2. Gör dom lika långa
% 3. Dela upp i N segment
% 4. skicka segmenten till N olika workers
% 5. utför beräknigar
% 6. skriv ut svar